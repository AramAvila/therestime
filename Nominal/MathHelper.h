#pragma once
#include <math.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <utility>

#include "gl/glm/glm.hpp"

class MathHelper {

public:
	static const double PI;

	static double getVectorLength(glm::vec3 vector);
	//static glm::vec3 getEntityMovement(Object * ent);
	static glm::vec3 getVectorDirection(glm::vec3 disp);
	static glm::ivec3 getVectorDirectionCustomAxis(glm::vec3 disp, glm::vec3 axisX, glm::vec3 axisY, glm::vec3 axisZ);
	static glm::ivec3 getVectorQuadrant(glm::vec3 disp);
	static glm::ivec3 getVectorQuadrantCustomAxis(glm::vec3 disp, glm::vec3 axisX, glm::vec3 axisY, glm::vec3 axisZ);
	static float angleBetween(glm::vec3 a, glm::vec3 b);
	static float angleBetweenNormal(glm::vec3 x, glm::vec3 y, glm::vec3 n);
	static float distancePointToLine(glm::vec3 point, glm::vec3 lineStart, glm::vec3 vector);
	static glm::vec3 shortestVecToLine(glm::vec3 VecPlane1, glm::vec3 VecPlane2, glm::vec3 VecLine, glm::vec3 PlanePoint, glm::vec3 LinePoint);
	static glm::vec3 changeReferenceSystem(glm::vec3 point, glm::vec3 newOrigin, glm::vec3 newX, glm::vec3 newY, glm::vec3 newZ);
	static glm::vec3 leaveReferenceSystem(glm::vec3 point, glm::vec3 origin, glm::vec3 vecX, glm::vec3 vecY, glm::vec3 vecZ);
	static glm::vec3 rotatePointArroundAxis(glm::vec3 point, glm::vec3 axis, double angle);
	static std::pair<double, double> getAnglesFromAim(glm::vec3 viewAxis);
	static double findRoll(glm::vec3 axisUp, glm::vec3 axisFront);
	static bool isInsideParalelogram(glm::vec3 point, glm::vec3 parStart, glm::vec3 parEnd);
	static glm::vec3 findVectorPointToParalelogram(glm::vec3 point, glm::vec3 polStart, glm::vec3 polEnd);
	static glm::vec3 getClosestPointToPolygon(glm::vec3 polStart, glm::vec3 polEnd, glm::vec3 relPosition, glm::vec3 relFront);
	static glm::vec3 snapPoint(glm::vec3 pointToSnap, float gridSize);
	static float distancePointToPlane(glm::vec3 relPosition, float A, float B, float C, float D);
	static float isLeft(glm::vec2 P0, glm::vec2 P1, glm::vec2 P2);
	static int simpleHull_2D(std::vector<glm::vec2> * P, std::vector<glm::vec2> * H);
	static std::vector<std::string> MathHelper::explode(const std::string& s, const char& c);
};