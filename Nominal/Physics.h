#pragma once
#include <vector>
#include <algorithm>
#include <iostream>
#include "gl/glm/vec2.hpp"
#include "gl/glm/glm.hpp"

#include "MathHelper.h"
#include "Triangle.h"
#include "Level.h"
#include "SceneManager.h"
#include "Player.h"
#include "Platform.h"

enum CollisionType {
	COLLIDE,
	TOUCH,
	NO_COLLISION
};

class Physics
{
public:
	Physics();
	~Physics();
	static Level * ProcessInstant(Level * instant);
	static bool ProcessPlayerInstant(Player * active_player, glm::vec3 player_action, Level * player_instant, float time_passed);
	static void calculate2dColision(glm::vec2 origin, glm::vec2 move_vec, glm::vec2 momentum, Level * current_instant, bool reverse_time = false);
	static glm::vec2 processIntersection(glm::vec2 p, glm::vec2 q, glm::vec2 r, glm::vec2 s);
	static bool onSegment(glm::vec2 p, glm::vec2 q, glm::vec2 r);
	static int orientation(glm::vec2 p, glm::vec2 q, glm::vec2 r);
	static bool doIntersect(glm::vec2 p1, glm::vec2 q1, glm::vec2 ref1, glm::vec2 p2, glm::vec2 q2, glm::vec2 ref2, bool extra_check = false);
	static glm::vec2 lineLineIntersection(glm::vec2 A, glm::vec2 B, glm::vec2 C, glm::vec2 D);

	static bool test();

private:
	static bool isTouching(glm::vec2 position, std::vector<glm::vec2> * outer_hull, std::vector<Triangle> * solid_list, glm::vec2 solid_position = glm::vec2(0, 0));
	static bool isOnTop(glm::vec2 position, std::vector<glm::vec2> * outer_hull, std::vector<Triangle> * solid_list, glm::vec2 solid_position = glm::vec2(0,0));
};

