#include "Physics.h"

Physics::Physics()
{
}


Physics::~Physics()
{
}

Level * Physics::ProcessInstant(Level * instant)
{

	glm::vec2 initial_momentum = instant->flatter_momentum;
	glm::vec2 end_momentum = initial_momentum + instant->gravity * instant->instant_length;

	instant->flatter_status = FLATTER_STATUS::ALIVE;
	for (std::vector<DeathSurface>::iterator ds = instant->deathSurface_list.begin(); ds != instant->deathSurface_list.end(); ds++) {
		if (Physics::isTouching(instant->flatter_position, &instant->flatter_flatterOuterHull, &ds->solid_list)) {
			instant->flatter_status = FLATTER_STATUS::DEAD;
		}
	}

	//Primer calculem com es mour� la plataforma i despr�s assignarem el moviment. Aix� podrem calcular m�s f�cilment les colisions entre el flatter i les plataformes
	for (std::vector<Platform>::iterator pl = instant->platform_list.begin(); pl != instant->platform_list.end(); pl++) {
		pl->move_vec = (pl->end_position - pl->init_position) * pl->speed * (float)pl->direction;

		pl->cycle += pl->speed * pl->direction;
		if (pl->cycle > 1) {
			pl->direction = -1;
		}
		else if (pl->cycle < 0)
		{
			pl->direction = 1;
		}

	}

	if (instant->flatter_status != FLATTER_STATUS::DEAD) {
		if (instant->action == ACTION_LIST::JUMP && Physics::isTouching(instant->flatter_position, &instant->flatter_flatterOuterHull, &instant->solid_list)) {
			end_momentum.y += instant->flatter_jump;
		}
		else
		{
			bool stop_loop = false;
			//Comprovem que pugui saltar per estar tocant alguna de les plataformes
			for (std::vector<Platform>::iterator pl = instant->platform_list.begin(); pl != instant->platform_list.end() && !stop_loop; pl++) {
				if (instant->action == ACTION_LIST::JUMP && Physics::isTouching(instant->flatter_position, &instant->flatter_flatterOuterHull, &pl->solid_list, pl->current_position)) {
					end_momentum.y += instant->flatter_jump;
					stop_loop = true;
				}
			}
		}


		glm::vec2 initial_position = instant->flatter_position;
		//Pf = Pi + Vi*t + 1/2*a*t^2;
		glm::vec2 end_position = initial_position +
			end_momentum * instant->instant_length +
			instant->gravity * pow(instant->instant_length, 2) / 2.0f;

		if (instant->order == ORDER_LIST::FORWARDS) {
			end_position.x += instant->flatter_speed;
		}

		if (instant->order == ORDER_LIST::BACKWARDS) {
			end_position.x -= instant->flatter_speed;
		}

		Physics::calculate2dColision(initial_position, end_position, end_momentum, instant);
	}
	
	for (std::vector<Platform>::iterator pl = instant->platform_list.begin(); pl != instant->platform_list.end(); pl++) {
		pl->current_position += pl->move_vec;
	}

	if (glm::length(instant->flatter_position - instant->goal_position) < 0.2) {
		instant->flatter_has_reached_end = true;
	}
	else {
		instant->flatter_has_reached_end = false;
	}

	//instant->flatter_has_reached_end = Physics::isTouching(instant->flatter_position, &instant->flatter_flatterOuterHull, &instant->solid_list, instant->goal_position);

	return instant;
}

bool Physics::ProcessPlayerInstant(Player * player, glm::vec3 player_order, Level * instant, float time_passed)
{
	glm::vec3 initial_momentum = player->Momentum;
	glm::vec3 end_momentum = initial_momentum + glm::vec3(instant->gravity.x, instant->gravity.y, 0) * time_passed;
	
	//Comprovem que pugui saltar per estar tocant el nivell
	if (player_order.z == 1 && Physics::isOnTop(glm::vec2(player->Position.x, player->Position.y), &instant->flatter_flatterOuterHull, &instant->solid_list)) {
		end_momentum.y += player->PlayerJump;
	}
	else
	{
		bool stop_loop = false;
		//Comprovem que pugui saltar per estar tocant alguna de les plataformes
		for (std::vector<Platform>::iterator pl = instant->platform_list.begin(); pl != instant->platform_list.end() && !stop_loop; pl++) {
			if (player_order.z == 1 && Physics::isTouching(glm::vec2(player->Position.x, player->Position.y), &instant->flatter_flatterOuterHull, &pl->solid_list, pl->current_position)) {
				end_momentum.y += player->PlayerJump;
				stop_loop = true;
			}
		}
	}

	//Pf = Pi + Vi*t + 1/2*a*t^2;
	glm::vec3 end_position = player->Position +
		end_momentum * time_passed +
		glm::vec3(instant->gravity.x, instant->gravity.y, 0) * pow(time_passed, 2) / 2.0f;

	//end_position = player->Position;

	glm::vec3 final_vec = player->Front * player_order.x + player->Right * player_order.y;

	if (glm::length(final_vec) != 0) {
		final_vec = glm::normalize(final_vec);
	}

	final_vec.y = 0;

	end_position += final_vec * player->Velocity ;

	glm::vec2 flatter_momentum_bak = instant->flatter_momentum;
	glm::vec2 flatter_position_bak = instant->flatter_position;

	bool reverse_time = false;
	//Si la z actual �s m�s gran que la z final voldr� dir que estem "Retrocedint" i haurem d'ajustar els calculs de les fisiques.
	if (player->Position.z > end_position.z) {
		reverse_time = true;
	}
	Physics::calculate2dColision(
		glm::vec2(player->Position.x, player->Position.y),
		glm::vec2(end_position.x, end_position.y),
		glm::vec2(end_momentum.x, end_momentum.y),
		instant,
		reverse_time
	);

	player->Momentum = glm::vec3(instant->flatter_momentum.x, instant->flatter_momentum.y, end_momentum.z);
	player->Position = glm::vec3(instant->flatter_position.x, instant->flatter_position.y, end_position.z);

	instant->flatter_momentum = flatter_momentum_bak;
	instant->flatter_position = flatter_position_bak;

	if (glm::length(glm::vec2(end_position.x, end_position.y) - instant->goal_position) < 0.2) {
		return true;
	}
	else {
		return false;
	}
	//return Physics::isTouching(glm::vec2(end_position.x, end_position.y), &instant->flatter_flatterOuterHull, &instant->solid_list, instant->goal_position);
}

/// <summary>
/// Determina si el flatter est� tocant qualsevol superf�cie i per tant, pot saltar.
/// </summary>
/// <param name="current_level">The current_level.</param>
/// <returns></returns>
bool Physics::isTouching(glm::vec2 position, std::vector<glm::vec2> * outer_hull, std::vector<Triangle> * solid_list, glm::vec2 solid_position)
{

	for (int i = 0; i < outer_hull->size(); i++)
	{

		for (std::vector<Triangle>::iterator tr = solid_list->begin(); tr != solid_list->end(); tr++) {
			if (
				Physics::onSegment(solid_position + tr->point_1, position + outer_hull->at(i), solid_position + tr->point_2) ||
				Physics::onSegment(solid_position + tr->point_2, position + outer_hull->at(i), solid_position + tr->point_3) ||
				Physics::onSegment(solid_position + tr->point_3, position + outer_hull->at(i), solid_position + tr->point_1))
			{
				return true;
			}
		}

	}

	return false;
}

/// <summary>
/// Determina si la closca exterior que s'envia �s a sobre de la superf�cie
/// </summary>
/// <param name="position">The position.</param>
/// <param name="outer_hull">The outer_hull.</param>
/// <param name="solid_list">The solid_list.</param>
/// <param name="solid_position">The solid_position.</param>
/// <returns></returns>
bool Physics::isOnTop(glm::vec2 position, std::vector<glm::vec2> * outer_hull, std::vector<Triangle> * solid_list, glm::vec2 solid_position)
{

	for (int i = 0; i < outer_hull->size(); i++)
	{

		for (std::vector<Triangle>::iterator tr = solid_list->begin(); tr != solid_list->end(); tr++) {
			if (Physics::onSegment(solid_position + tr->point_1, position + outer_hull->at(i), solid_position + tr->point_2)) {
				if (solid_position.y + tr->point_1.y - solid_position.y - tr->point_2.y == 0) {
					return true;
				}
			}

			if (Physics::onSegment(solid_position + tr->point_2, position + outer_hull->at(i), solid_position + tr->point_3)) {
				if (solid_position.y + tr->point_2.y - solid_position.y - tr->point_3.y == 0) {
					return true;
				}
			}

			if(Physics::onSegment(solid_position + tr->point_3, position + outer_hull->at(i), solid_position + tr->point_1)){
				if (solid_position.y + tr->point_3.y - solid_position.y - tr->point_1.y == 0) {
					return true;
				}
			}
		}

	}

	return false;
}


void Physics::calculate2dColision(glm::vec2 initial_position, glm::vec2 final_position, glm::vec2 momentum, Level * current_level, bool reverse_time)
{
	current_level->debug_render_text.clear();

	std::vector<glm::vec2> outer_hull = current_level->flatter_flatterOuterHull;
	std::vector<Triangle> solid_list = current_level->solid_list;

	//Afegim les zones de mort als solids amb els que es pot colisionar.
	for (std::vector<DeathSurface>::iterator ds = current_level->deathSurface_list.begin(); ds != current_level->deathSurface_list.end(); ds++) {
		solid_list.insert(solid_list.begin(), ds->solid_list.begin(), ds->solid_list.end());
	}

	glm::vec2
		move_vec = final_position - initial_position,
		flatter_center = current_level->flatter_center + initial_position;

	glm::vec2 init_point, end_point;
	current_level->debug_render_text.push_back(std::make_pair(current_level->flatter_position + glm::vec2(0, 1.0f), std::to_string(initial_position.x) + ", " + std::to_string(initial_position.y)));

	for (std::vector<Platform>::iterator pl = current_level->platform_list.begin(); pl != current_level->platform_list.end(); pl++) {

		for each (Triangle tr in pl->solid_list)
		{
			Triangle t_prima = Triangle(
				tr.point_1 + pl->current_position,
				tr.point_2 + pl->current_position,
				tr.point_3 + pl->current_position
			);
			solid_list.push_back(t_prima);
		}

	}

	for (int i = 0; i < outer_hull.size(); i++)
	{
		init_point = outer_hull[i] + initial_position;
		end_point = outer_hull[i] + initial_position + move_vec;
		if (outer_hull[i].y == 0) {
			int a = 1 + 1;
		}
		for (std::vector<Triangle>::iterator tr = solid_list.begin(); tr != solid_list.end(); tr++) {


			if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_1, tr->point_2, tr->point_3)) {
				move_vec = Physics::processIntersection(init_point, end_point, tr->point_1, tr->point_2);
				end_point = outer_hull[i] + initial_position + move_vec;
				current_level->debug_render_text.push_back(std::make_pair(tr->point_1 + glm::vec2(0,0.1f), "S_1"));
				current_level->debug_render_text.push_back(std::make_pair(tr->point_2 + glm::vec2(0, 0.1f), "S_2"));
			}

			if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_2, tr->point_3, tr->point_1)) {
				move_vec = Physics::processIntersection(init_point, end_point, tr->point_2, tr->point_3);
				end_point = outer_hull[i] + initial_position + move_vec;
				current_level->debug_render_text.push_back(std::make_pair(tr->point_2 + glm::vec2(0, 0.1f), "S_2"));
				current_level->debug_render_text.push_back(std::make_pair(tr->point_3 + glm::vec2(0, 0.1f), "S_3"));
			}

			if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_3, tr->point_1, tr->point_2)) {
				move_vec = Physics::processIntersection(init_point, end_point, tr->point_3, tr->point_1);
				end_point = outer_hull[i] + initial_position + move_vec;
				current_level->debug_render_text.push_back(std::make_pair(tr->point_3 + glm::vec2(0, 0.1f), "S_3"));
				current_level->debug_render_text.push_back(std::make_pair(tr->point_1 + glm::vec2(0, 0.1f), "S_1"));
			}

		}
	}

	
	if (final_position.x - initial_position.x - move_vec.x != 0) {
		momentum.x = 0;
	}

	if (final_position.y - initial_position.y - move_vec.y != 0) {
		momentum.y = 0;
	}

	current_level->flatter_momentum = momentum;
	current_level->flatter_position = initial_position + move_vec;

	flatter_center = current_level->flatter_center + current_level->flatter_position;

	current_level->debug_render_text.push_back(std::make_pair(current_level->flatter_position + glm::vec2(0, 0.9f), std::to_string(current_level->flatter_position.x) + ", " + std::to_string(current_level->flatter_position.y)));

	/*
	 * Calculem els moviments de les plataformes i les possibles colisions
	 */
	for (std::vector<Platform>::iterator pl = current_level->platform_list.begin(); pl != current_level->platform_list.end(); pl++) {

		if (reverse_time) {
			move_vec = pl->move_vec;
		}
		else {
			move_vec = -pl->move_vec;
		}

		bool pl_collision = false;

		for (int i = 0; i < outer_hull.size(); i++)
		{
			init_point = outer_hull[i] + current_level->flatter_position;
			end_point = outer_hull[i] + current_level->flatter_position + move_vec;

			for (std::vector<Triangle>::iterator tr = pl->solid_list.begin(); tr != pl->solid_list.end(); tr++) {

				if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_1 + pl->current_position, tr->point_2 + pl->current_position, tr->point_3 + pl->current_position)) {
					
					current_level->debug_render_text.push_back(std::make_pair(tr->point_1 + pl->current_position + glm::vec2(0, 0.2f), "P_1"));
					current_level->debug_render_text.push_back(std::make_pair(tr->point_2 + pl->current_position + glm::vec2(0, 0.2f), "P_2"));
					
					move_vec = Physics::processIntersection(init_point, end_point, tr->point_1 + pl->current_position, tr->point_2 + pl->current_position);
					end_point = outer_hull[i] + initial_position + move_vec;
					pl_collision = true;
				}

				if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_2 + pl->current_position, tr->point_3 + pl->current_position, tr->point_1 + pl->current_position)) {

					current_level->debug_render_text.push_back(std::make_pair(tr->point_2 + pl->current_position + glm::vec2(0, 0.2f), "P_2"));
					current_level->debug_render_text.push_back(std::make_pair(tr->point_3 + pl->current_position + glm::vec2(0, 0.2f), "P_3"));
					
					move_vec = Physics::processIntersection(init_point, end_point, tr->point_2 + pl->current_position, tr->point_3 + pl->current_position);
					end_point = outer_hull[i] + initial_position + move_vec;
					pl_collision = true;
				}

				if (glm::length(move_vec) > 0 && Physics::doIntersect(init_point, end_point, flatter_center, tr->point_3 + pl->current_position, tr->point_1 + pl->current_position, tr->point_2 + pl->current_position)) {

					current_level->debug_render_text.push_back(std::make_pair(tr->point_3 + pl->current_position + glm::vec2(0, 0.2f), "P_3"));
					current_level->debug_render_text.push_back(std::make_pair(tr->point_1 + pl->current_position + glm::vec2(0, 0.2f), "P_1"));
					
					move_vec = Physics::processIntersection(init_point, end_point, tr->point_3 + pl->current_position, tr->point_1 + pl->current_position);
					end_point = outer_hull[i] + initial_position + move_vec;
					pl_collision = true;
				}
			}
		}

		if (pl_collision) {

			if (final_position.x - initial_position.x - move_vec.x != 0) {
				momentum.x = 0;
			}

			if (final_position.y - initial_position.y - move_vec.y != 0) {
				momentum.y = 0;
			}

			current_level->flatter_momentum = momentum;

			if (reverse_time) {
				current_level->flatter_position -= pl->move_vec + move_vec;
			}
			else {
				current_level->flatter_position += pl->move_vec + move_vec;
			}
			current_level->debug_render_text.push_back(std::make_pair(pl->current_position + glm::vec2(0, 0.3f), std::to_string(pl->move_vec.x + move_vec.x) + ", " + std::to_string(pl->move_vec.y + move_vec.y)));
			current_level->debug_render_text.push_back(std::make_pair(current_level->flatter_position + glm::vec2(0, 0.8f), std::to_string(current_level->flatter_position.x) + ", " + std::to_string(current_level->flatter_position.y)));

		};
	}
}

glm::vec2 Physics::processIntersection(glm::vec2 p, glm::vec2 q, glm::vec2 r, glm::vec2 s) {

	glm::vec2 pq, rs, sr;
	pq = q - p;
	rs = s - r;
	sr = s - r;

	float dot, det, angle_pqrs, angle_pqsr;
	dot = pq.x*rs.x + pq.y*rs.y;
	det = pq.x*rs.y - pq.y*rs.x;
	angle_pqrs = atan2(det, dot);

	//Les dues l�nies son pr�cticament paraleles
	if (abs(abs(angle_pqrs) - MathHelper::PI) < 0.001 || abs(angle_pqrs) < 0.001) {
		if (glm::length(p - r) > glm::length(p - s)) {
			return s - p;
		}
		else {
			return r - p;
		}
	}
	else {
		glm::vec2 meet_point = Physics::lineLineIntersection(p, q, r, s);

		glm::vec2 a, b;
		a = q - meet_point;
		b = s - r;

		float proj = ((a.x * b.x + a.y * b.y) / glm::length(b));
		glm::vec2 result = glm::normalize(b) * proj;

		/*dot = pq.x*sr.x + pq.y*sr.y;
		det = pq.x*sr.y - pq.y*sr.x;
		angle_pqsr = atan2(det, dot);

		if (angle_pqrs > angle_pqsr) {
			result = -result;
		}

		if (a.x >= 0) {
			result.x = abs(result.x);
		}
		else {
			result.x = -abs(result.x);
		}

		if (a.y >= 0) {
			result.y = abs(result.y);
		}
		else {
			result.y = -abs(result.y);
		}*/
		glm::vec2 res = (meet_point + result) - p;
		//glm::vec2 res = meet_point - p;
		return res;
	}


}



//EXTRET DE https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
//EXTRET DE https://wikimedia.org/api/rest_v1/media/math/render/svg/be2ab4a9d9d77f1623a2723891f652028a7a328d
//EL DIA 2019/04/05
//Comprova si el punt q pertany a la linia pr
bool Physics::onSegment(glm::vec2 p, glm::vec2 q, glm::vec2 r)
{
	/*if (q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) &&
		q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y))
		return true;*/

	// Return minimum distance betreen line segment pr and qoint q
	float length;
	float l2 = pow(glm::length(p - r), 2);  // i.e. |r-p|^2 -  apoid a sqrt
	if (l2 == 0.0) {
		length = glm::length(q - p);
	}
	else {
		float t = std::max(0.0f, std::min(1.0f, glm::dot(q - p, r - p) / l2));
		glm::vec2 qrojection = p + t * (r - p);  // Projection falls on the segment
		length = glm::length(q - qrojection);
	}

	if (abs(length) < 0.001) {
		return true;
	}
	else {
		return false;
	}
}

// To find orientation of ordered triplet (p, q, r). 
// The function returns following values 
// 0 --> p, q and r are colinear 
// 1 --> Clockwise 
// 2 --> Counterclockwise 
int Physics::orientation(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3)
{
	// See https://www.geeksforgeeks.org/orientation-3-ordered-points/ 
	// for details of below formula. 

	glm::vec2 pq = p2 - p1;
	glm::vec2 rs = p3 - p1;

	float dot, det, angle_pqrs, angle_pqsr;
	dot = pq.x*rs.x + pq.y*rs.y;
	det = pq.x*rs.y - pq.y*rs.x;
	angle_pqrs = atan2(det, dot);

	//Les dues l�nies son pr�cticament paraleles
	if (abs(abs(angle_pqrs) - MathHelper::PI) < 0.001 || abs(angle_pqrs) < 0.001) {
		return 0;
	}
	else {
		float val = (p2.y - p1.y) * (p3.x - p2.x) -
			(p2.x - p1.x) * (p3.y - p2.y);

		if (val == 0) return 0;  // colinear 

		return (val > 0) ? 1 : 2; // clock or counterclock wise 
	}
	
}

// The main function that returns true if line segment 'p1q1' 
// and 'p2q2' intersect. 
bool Physics::doIntersect(glm::vec2 p1, glm::vec2 q1, glm::vec2 ref1, glm::vec2 p2, glm::vec2 q2, glm::vec2 ref2, bool test)
{
	int o1 = orientation(p1, q1, p2);
	int o2 = orientation(p1, q1, q2);
	int o3 = orientation(p2, q2, p1);
	int o4 = orientation(p2, q2, q1);

	int o5 = orientation(p2, q2, ref1);
	int o6 = orientation(p2, q2, ref2);


	if (test) {
		std::cout << "o1 = " << o1 << " / ";
		std::cout << "o2 = " << o2 << " / ";
		std::cout << "o3 = " << o3 << " / ";
		std::cout << "o4 = " << o4 << " / ";
		std::cout << "o5 = " << o5 << " / ";
		std::cout << "o6 = " << o6 << " / ";
	}

	// General case 
	if (o1 != 0 &&  //p1, q1, p2 no son colineals
		o2 != 0 &&  //p1, q1, q2 no son colineals
		(o3 == 0 && o6 == o4 || o3 != 0) &&  //p2, q2, p1 no son colineals o si ho son, el moviment es en el sentit de ref2
		o1 != o2 && //p2 i q2 son en bandes oposades de p1q1
		o3 != o4 && //p1 i q1 son en bandes oposades de p2q2
		o5 != o6 && //ref1 i ref2 son en bandes oposades de p2q2
		o3 != o6    //ref1 i ref2 son en bandes oposades de p2q2
		) {
		if (test) {
			std::cout << " GC ";
		}
		return true; //true
	}

	// Special Cases 
	// p1, q1 and p2 are colinear and p2 lies on segment p1q1 
	//if (o1 == 0 && onSegment(p1, p2, q1)) return 2;  //true

	// p1, q1 and q2 are colinear and q2 lies on segment p1q1 
	//if (o2 == 0 && onSegment(p1, q2, q1)) return 3;  //true

	// p2, q2 and p1 are colinear and p1 lies on segment p2q2 
	//if (o3 == 0 && o5 == o6 && onSegment(p2, p1, q2)) return 4;  //true

	// p2, q2 and q1 are colinear and q1 lies on segment p2q2 
	if (o4 == 0 && o5 == o6 && onSegment(p2, q1, q2)) {
		if (test) {
			std::cout << " SC ";
		}
		return true;  //true
	}

	return false; // Doesn't fall in any of the above cases FALSE
}

glm::vec2 Physics::lineLineIntersection(glm::vec2 A, glm::vec2 B, glm::vec2 C, glm::vec2 D)
{
	// Line AB represented as a1x + b1y = c1 
	double a1 = B.y - A.y;
	double b1 = A.x - B.x;
	double c1 = a1*(A.x) + b1*(A.y);

	// Line CD represented as a2x + b2y = c2 
	double a2 = D.y - C.y;
	double b2 = C.x - D.x;
	double c2 = a2*(C.x) + b2*(C.y);

	double determinant = a1*b2 - a2*b1;

	if (determinant == 0)
	{
		// The lines are parallel. This is simplified 
		// by returning a pair of FLT_MAX 
		return glm::vec2(FLT_MAX, FLT_MAX);
	}
	else
	{
		double x = (b2*c1 - b1*c2) / determinant;
		double y = (a1*c2 - a2*c1) / determinant;
		return glm::vec2(x, y);
	}
}

bool Physics::test() {

	glm::vec2 p1 = { 0, 0 }, q1 = { 2, 0 }, ref1 = { -1, 0 };
	glm::vec2 p2 = { 1, 0 }, q2 = { 1, 1 }, ref2 = { 0, 1 };
	std::cout << "    Q1      \n";
	std::cout << "    |       \n";
	std::cout << "    |P2     \n";
	std::cout << "    +-----Q2\n";
	std::cout << "    |       \n";
	std::cout << "    |       \n";
	std::cout << "R1  P1    R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	glm::vec2 res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 0, 0 }, q1 = { 2, 0 }, ref1 = { 2, 1 };
	p2 = { 1, 0 }, q2 = { 1, 1 }, ref2 = { 0, 1 };
	std::cout << "    Q1    R1\n";
	std::cout << "    |       \n";
	std::cout << "    |P2     \n";
	std::cout << "    +-----Q2\n";
	std::cout << "    |       \n";
	std::cout << "    |       \n";
	std::cout << "    P1    R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 1, 0 }, q1 = { 1, 2 }, ref1 = { 2,  2 };
	p2 = { 0, 0 }, q2 = { 2, 0 }, ref2 = { 0, -1 };
	std::cout << "      Q1   R1\n";
	std::cout << "      |      \n";
	std::cout << "      |      \n";
	std::cout << "      |      \n";
	std::cout << "      |      \n";
	std::cout << "P2----+----Q2\n";
	std::cout << "      P1   R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 1, 0 }, q1 = { 1, -2 }, ref1 = { 2,  2 };
	p2 = { 0, 0 }, q2 = { 2, 0 }, ref2 = { 0, -1 };
	std::cout << "      P1   R1\n";
	std::cout << "P2----+----Q2\n";
	std::cout << "      |      \n";
	std::cout << "      |      \n";
	std::cout << "R2    |      \n";
	std::cout << "      |      \n";
	std::cout << "      Q1     \n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "OK" : "KO");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";


	p1 = { 0, 1 }, q1 = { 3, 1 }, ref1 = { 0, 0 };
	p2 = { 2, 0 }, q2 = { 2, 4 }, ref2 = { 3, 0 };
	std::cout << "     P2    \n";
	std::cout << "     |     \n";
	std::cout << "     |     \n";
	std::cout << "P1---+---Q1\n";
	std::cout << "     |     \n";
	std::cout << "     |     \n";
	std::cout << "R1   Q2  R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "OK" : "KO");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 0, 1 }, q1 = { 3, 1 }, ref1 = { 0, 0 };
	p2 = { 2, 0 }, q2 = { 2, 4 }, ref2 = { 0, 4 };
	std::cout << "R2   P2    \n";
	std::cout << "     |     \n";
	std::cout << "     |     \n";
	std::cout << "P1---+---Q1\n";
	std::cout << "     |     \n";
	std::cout << "     |     \n";
	std::cout << "R1   Q2    \n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";


	p1 = { 0, 0 }, q1 = { 2, 0 }, ref1 = { 1, -1 };
	p2 = { 1, 0 }, q2 = { 3, 0 }, ref2 = { 2, -1 };
	std::cout << "P1---P2---Q1---Q2\n";
	std::cout << "   R1        R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "OK" : "KO");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 0, 0 }, q1 = { 2, 0 }, ref1 = { 1,  1 };
	p2 = { 1, 0 }, q2 = { 3, 0 }, ref2 = { 2, -1 };
	std::cout << "   R1\n";
	std::cout << "P1---P2---Q1---Q2\n";
	std::cout << "            R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 2, 0 }, q1 = { 0, 0 }, ref1 = { 1,  1 };
	p2 = { 1, 0 }, q2 = { 3, 0 }, ref2 = { 2, -1 };
	std::cout << "Q1---P2---P1---Q2\n";
	std::cout << "   R1        R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 2, 0 }, q1 = { 0, 0 }, ref1 = { 1,  1 };
	p2 = { 1, 0 }, q2 = { 3, 0 }, ref2 = { 2, -1 };
	std::cout << "   R1\n";
	std::cout << "Q1---P2---P1---Q2\n";
	std::cout << "            R2\n";
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "KO" : "OK");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";

	p1 = { 0.259578466, 1.09999990  }, q1 = { 0.259578466, 1.06321 }, ref1 = { 0.5, 1.39 };
	p2 = { 1.00000000, 1.10000002 }, q2 = { -1.00000000, 1.10000002 }, ref2 = { 1, 0.5 };
	std::cout << " = " << (Physics::doIntersect(p1, q1, ref1, p2, q2, ref2, true) ? "OK" : "KO");
	res = Physics::processIntersection(p1, q1, p2, q2);
	std::cout << " -- " << glm::vec2(q1 - p1).x << ", " << glm::vec2(q1 - p1).y << " => " << res.x << ", " << res.y << "\n";
	std::cout << "===========================\n";
	return true;
}