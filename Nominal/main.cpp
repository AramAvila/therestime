
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <memory>

// GLM Mathematics
#include "gl/glm/glm.hpp"
#include "gl/glm/gtc/matrix_transform.hpp"
#include "gl/glm/gtc/type_ptr.hpp"

#include "Renderer.h"
#include "SceneManager.h"
#include "Player.h"
#include "Physics.h"
#include "Hud.h"

void processKeyInput(GLFWwindow *window, int key, int scancode, int action, int mode);
void processMouseInput(GLFWwindow* window, double xpos, double ypos);
void processScrollInput(GLFWwindow* window, double xoffset, double yoffset);

void loadLevel(std::string level_name);

Renderer * renderer_obj;
SceneManager * scene_manager;
Hud * hud;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

float lastX, lastY;
bool firstMouse;

std::vector<std::string> level_list = {
	"level1.lvl",
	"level2.lvl",
	"level3.lvl",
	"level4.lvl",
	"level5.lvl",
	"level6.lvl",
	"level7.lvl",
	"level8.lvl",
	"level9.lvl",
	"level10.lvl",
	"level11.lvl",
	"level12.lvl",
	"level13.lvl",
	"level14.lvl",
	"level15.lvl"
};

/*std::vector<std::string> level_list = {
	"test1.lvl",
	"test2.lvl",
	"level3.lvl",
	"level4.lvl",
	"level5.lvl",
	"level6.lvl",
	"level7.lvl",
	"level8.lvl",
	"level9.lvl",
	"level10.lvl"
};*/

enum RENDER_SET {
	WELCOME_SCREEN,
	LEVEL_INIT,
	REGULAR_LEVEL,
	LEVEL_COMPLETE,
	ENDGAME
};

RENDER_SET active_rendering = WELCOME_SCREEN;

int current_level = 0;

int main(){

	/*
	std::cout << Physics::test();

	std::cout << "There's time, Aram �vila Salvad�, TFG" << std::endl;
	std::cout << "Mou-te per l'espai de 4D utilitzant WASD i salta prement ESPAI." << std::endl;
	std::cout << "Dona ordres a la criatura tridimensional amb Q, E, R, F, G." << std::endl;
	std::cout << "Experimenta amb els moviments i aconsegueix fer arribar a la criatura a l'altra banda del nivell." << std::endl << std::endl;;
	*/
	//system("pause");

	glewExperimental = GL_TRUE;
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	glEnable(GL_MULTISAMPLE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* window = glfwCreateWindow(Renderer::SCR_WIDTH, Renderer::SCR_HEIGHT, "There's Time", nullptr, nullptr); // Windowed;

	glfwMakeContextCurrent(window);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	glfwSetKeyCallback(window, processKeyInput);
	glfwSetCursorPosCallback(window, processMouseInput);

	//glfwSetMouseButtonCallback(window, mouse_button_callback);
	//glfwSetScrollCallback(window, scroll_callback);

	glViewport(0, 0, Renderer::SCR_WIDTH, Renderer::SCR_HEIGHT);

	renderer_obj = new Renderer();
	renderer_obj->window = window;
	renderer_obj->setUpTextRendering();

	hud = new Hud();
	hud->texturePack = renderer_obj->loadTexture("textures/Textures Bright Colors Noise.png");

	scene_manager = new SceneManager();

	loadLevel(level_list.at(current_level));

	// Game loop
	while (!glfwWindowShouldClose(renderer_obj->window))
	{
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		scene_manager->delta_time = deltaTime;

		if (active_rendering == WELCOME_SCREEN) {
			scene_manager->active_player.Position.x = scene_manager->current_level.player_starting_position.x + sin(currentFrame / 50);
		}

		if (active_rendering == REGULAR_LEVEL) {
			scene_manager->ProcessPlayerPhysics();
			scene_manager->UpdateInstant();
		}

		// Check and call events
		glfwPollEvents();

		renderer_obj->renderScene(scene_manager, active_rendering);

		if (scene_manager->active_player.Position.y < scene_manager->current_level.player_fall_reset_threshold) {
			scene_manager->active_player.Position = glm::vec3(scene_manager->current_level.player_starting_position.x, scene_manager->current_level.player_starting_position.y, 3);
			
			scene_manager->active_player.Pitch = 0;
			scene_manager->active_player.Yaw = 0;

			scene_manager->active_player.updateCameraVectors();
			//scene_manager->ResetActions();
			//scene_manager->UpdateTimeline();
		}
		
		/*
		 * Condici� per completar el nivell
		 */
		if (scene_manager->player_completed && scene_manager->flatter_completed && active_rendering != ENDGAME) {
			active_rendering = LEVEL_COMPLETE;
			scene_manager->active_player.Yaw = 90;
			scene_manager->active_player.Pitch = 0;
			scene_manager->active_player.updateCameraVectors();
		}
	}

	//Destru�m l'objecte del renderer abans de tancar per assegurar-nos que no es queden elements en la mem�ria
	renderer_obj->~Renderer();

	return 0;
}

void loadLevel(std::string level_name) {

	scene_manager->LoadLevel(level_list.at(current_level));

	scene_manager->active_player = Player(glm::vec3(scene_manager->current_level.player_starting_position.x, scene_manager->current_level.player_starting_position.y, 2), glm::vec3(0, 1, 0), 90, 10);
	scene_manager->active_player.displacement = glm::vec3(scene_manager->current_level.flatter_center.x, scene_manager->current_level.flatter_center.y * 1.5f, -0.1f);

	renderer_obj->updateLevelModel(&scene_manager->current_level);

	scene_manager->UpdateTimeline();
}

void processKeyInput(GLFWwindow* window, int key, int scancode, int action, int mode) {

	if (active_rendering == WELCOME_SCREEN && action == GLFW_PRESS) {
		active_rendering = REGULAR_LEVEL;
	}

	if (active_rendering == ENDGAME && action == GLFW_PRESS) {
		active_rendering = REGULAR_LEVEL;
		current_level = 0;
		loadLevel(level_list.at(current_level));
	}


	if (active_rendering == LEVEL_COMPLETE && action == GLFW_PRESS) {
		if (current_level < level_list.size() - 1) {
			active_rendering = REGULAR_LEVEL;
			loadLevel(level_list.at(++current_level));
		}
		else {
			active_rendering = ENDGAME;
		}

	}

	switch (key)
	{
		case 87:
			if (action == GLFW_PRESS) {
				scene_manager->player_order.x = 1;
			}
			else if(action == GLFW_RELEASE) {
				scene_manager->player_order.x = 0;
			}
			break;

		case 83:
			if (action == GLFW_PRESS) {
				scene_manager->player_order.x = -1;
			}
			else if (action == GLFW_RELEASE) {
				scene_manager->player_order.x = 0;
			}
			break;

		case 68:
			if (action == GLFW_PRESS) {
				scene_manager->player_order.y = 1;
			}
			else if (action == GLFW_RELEASE) {
				scene_manager->player_order.y = 0;
			}
			break;

		case 65:
			if (action == GLFW_PRESS) {
				scene_manager->player_order.y = -1;
			}
			else if (action == GLFW_RELEASE) {
				scene_manager->player_order.y = 0;
			}
			break;

		case 32:
			if (action == GLFW_PRESS) {
				scene_manager->player_order.z = 1;
			}
			else if (action == GLFW_RELEASE) {
				scene_manager->player_order.z= 0;
			}
			break;
		default:
			break;
	}

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);

	}
	else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else {
		scene_manager->processKeyInput(key, scancode, action, mode, deltaTime);
	};
}


void processMouseInput(GLFWwindow* window, double xpos, double ypos) {
	
	if (active_rendering == RENDER_SET::REGULAR_LEVEL) {
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

		lastX = xpos;
		lastY = ypos;


		scene_manager->ProcessMouseMovement(xoffset, yoffset);
	}

}

void processScrollInput(GLFWwindow* window, double xoffset, double yoffset) {

}