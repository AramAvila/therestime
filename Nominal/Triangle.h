#pragma once

#include "gl/glm/vec2.hpp"
#include "MathHelper.h"

class Triangle
{
public:
	Triangle();
	Triangle(glm::vec2 point_1, glm::vec2 point_2, glm::vec2 point_3);
	~Triangle();
	glm::vec2 point_1;
	glm::vec2 point_2;
	glm::vec2 point_3;
};

