#include "Hud.h"

Hud::Hud()
{
	float xStart = 0,
		xEnd = 26.0f / 28.0f,
		xStep = 1.0f / 28.0f,
		yStart = 12.0f / 16.0f,
		yEnd = 7.0f / 8.0f,
		yStep = 1.0f / 16.0f,
		width = this->CHARACTER_WIDTH,
		height = this->CHARACTER_HEIGHT;

	std::map<char, std::vector<GLfloat>> alphabet;

	char caracterList[44] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ',', '.', '-', '?', '%', '1','2','3','4','5','6','7','8','9','0' };

	int c = 0;
	for (float x = xStart; x < xEnd; x += xStep) {
		for (float y = yStart; y < yEnd; y += yStep) {

			std::vector<GLfloat> planeVertices = {
				// Positions			//Normals				// Texture Coords
				0.0f,	0.0f,	0.0f,	0.0f,  0.0f, -1.0f,		x,			y,	// Bottom-left	//1.0f, 1.0f, 
				width,	height, 0.0f,	0.0f,  0.0f, -1.0f,		x + xStep,	y + yStep,			// top-right	//0.0f, 0.0f, 
				width,	0.0f,	0.0f,	0.0f,  0.0f, -1.0f,		x + xStep,	y,	// bottom-right	//0.0f, 1.0f, 
				width,	height, 0.0f,	0.0f,  0.0f, -1.0f,		x + xStep,	y + yStep,			// top-right	//0.0f, 0.0f, 
				0.0f,	0.0f,	0.0f,	0.0f,  0.0f, -1.0f,		x ,	y ,	// bottom-left	//1.0f, 1.0f, 
				0.0f,	height, 0.0f,	0.0f,  0.0f, -1.0f,		x,	y + yStep,			// top-left		//1.0f, 0.0f, 
			};

			alphabet.insert(std::pair<char, std::vector<GLfloat>>(caracterList[c], planeVertices));
			c++;
		}
	}

	this->alphabet = alphabet;
}


Hud::~Hud()
{
}

void Hud::addToHUD(glm::vec2 pos, float value) {

	char characters[30];
	sprintf_s(characters, "%f", value);

	int c = 0;

	while (characters[c] != '\0' && characters[c] != '.') {
		this->charArray.push_back(std::pair<glm::vec2, std::vector<GLfloat>>(pos, this->alphabet.at(characters[c])));
		pos.x += this->CARACTER_KERNING;
		c++;
	}


	for (int a = 0; a <= this->HUD_PRECISION; a++) {
		this->charArray.push_back(std::pair<glm::vec2, std::vector<GLfloat>>(pos, this->alphabet.at(characters[c + a])));
		pos.x += this->CARACTER_KERNING;
	}
};