#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "gl/glm/vec3.hpp"
#include "gl/glm/vec2.hpp"

#include "Triangle.h"
#include "MathHelper.h"
#include "Platform.h"
#include "DeathSurface.h"

enum ORDER_LIST {
	FORWARDS,
	BACKWARDS,
	LEFT,
	RIGHT,
	STOP,
	ORDER_NONE,
};

enum ACTION_LIST {
	JUMP,
	ACTION_NONE
};

enum FLATTER_STATUS {
	ALIVE,
	DEAD
};

class Level
{
public:

	Level();
	Level(std::string level_name);
	~Level();

	std::string flatter_file = "flatter.obj";
	std::string level_file;

	std::vector<Platform> platform_list;
	std::vector<DeathSurface> deathSurface_list;
	float player_fall_reset_threshold;
	
	float instant_length;
	int level_length;
	glm::vec2 gravity;
	glm::vec2 goal_position;

	std::vector<int> distortion_init_list;
	std::vector<int> distortion_end_list;

	glm::vec2 player_starting_position;

	std::string name;
	glm::vec2 flatter_starting_position;
	glm::vec2 flatter_position;
	glm::vec2 flatter_momentum;
	glm::vec2 flatter_center;
	bool flatter_has_reached_end = false;

	ORDER_LIST order;
	ACTION_LIST action;
	FLATTER_STATUS flatter_status = FLATTER_STATUS::ALIVE;

	//Level
	int VAO, VBO, modelVAO, modelVBO, vertexCount;
	float modelVertices[10000];
	std::vector<Triangle> solid_list;
	std::vector<float> vertexValues;

	//Flatter -- Hauria d'anar a la seva pr�pia classe
	float flatter_modelVertices[10000];
	int flatter_VAO, flatter_VBO, flatter_modelVAO, flatter_modelVBO, flatter_vertexCount;
	float flatter_speed = 0.1f;
	float flatter_jump = 6.0f;
	std::vector<float> flatter_vertexValues;
	std::vector<glm::vec2> flatter_flatterOuterHull;

	//Arrow
	int arrow_VAO, arrow_VBO, arrow_modelVAO, arrow_modelVBO, arrow_vertexCount;
	float arrow_modelVertices[10000];
	std::vector<float> arrow_vertexValues;

	//absolute position in world space
	glm::vec3 absPosition = glm::vec3(0,0,0);

	void loadLevel(std::string path);

	void loadPlayer(std::string path);

	std::vector<std::pair<glm::vec2, std::string>> debug_render_text;
	std::vector<std::pair<glm::vec3, std::string>> level_text_list;

private:
	void loadObjectModel(std::string path, std::vector<Triangle> * local_solid_list, float * local_model_vertices, int * local_vertex_count, std::vector<float> * local_vertexValues);

};