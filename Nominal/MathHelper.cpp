#include "MathHelper.h"

using namespace std;

const double MathHelper::PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502;

double MathHelper::getVectorLength(glm::vec3 vector) {

	/*double dist = pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2);

	return sqrt(dist);*/

	return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}

/*glm::vec3 MathHelper::getEntityMovement(Object * ent) {
glm::vec3 move;
move.x = (ent->absPosition.x - ent->destination.x); //x
move.y = (ent->absPosition.y - ent->destination.y); //y
move.z = (ent->absPosition.z - ent->destination.z); //z

move = glm::normalize(move);
move.x = move.x * ent->speed;
move.y = move.y * ent->speed;
move.z = move.z * ent->speed;

return move;
}*/


/// <summary>
/// Returns the direction that a vector is pointing (up -> (0,1,0), front -> (1,0,0) ... ) 
/// </summary>
/// <param name="disp">Vector</param>
/// <returns></returns>
glm::vec3 MathHelper::getVectorDirection(glm::vec3 disp) {

	glm::vec3 orientation;

	double angleX = glm::degrees(angleBetween(disp, glm::vec3(1, 0, 0)));
	double angleY = glm::degrees(angleBetween(disp, glm::vec3(0, 1, 0)));
	double angleZ = glm::degrees(angleBetween(disp, glm::vec3(0, 0, 1)));

	if (angleX < 45) {
		orientation.x = 1;
	}
	else if (angleX > 135) {
		orientation.x = -1;
	}

	if (angleY < 45) {
		orientation.y = 1;
	}
	else if (angleY > 135) {
		orientation.y = -1;
	}

	if (angleZ < 45) {
		orientation.z = 1;
	}
	else if (angleZ > 135) {
		orientation.z = -1;
	}
	return orientation;
}

/// <summary>
/// Returns de direction that a vector is pointing. The axis that define "up, down" are defined by the 3 axis given
/// </summary>
/// <param name="disp">The disp.</param>
/// <param name="axisX">The axis x.</param>
/// <param name="axisY">The axis y.</param>
/// <param name="axisZ">The axis z.</param>
/// <returns></returns>
glm::ivec3 MathHelper::getVectorDirectionCustomAxis(glm::vec3 disp, glm::vec3 axisX, glm::vec3 axisY, glm::vec3 axisZ) {

	glm::ivec3 orientation;

	double angleX = glm::degrees(angleBetween(disp, axisX));
	double angleY = glm::degrees(angleBetween(disp, axisY));
	double angleZ = glm::degrees(angleBetween(disp, axisZ));

	double minusAngleX = glm::degrees(angleBetween(disp, -axisX));
	double minusAngleY = glm::degrees(angleBetween(disp, -axisY));
	double minusAngleZ = glm::degrees(angleBetween(disp, -axisZ));

	if (angleX < 45) {
		orientation.x = 1;
	}
	else if (angleX > 135) {
		orientation.x = -1;
	}

	if (angleY < 45) {
		orientation.y = 1;
	}
	else if (angleY > 135) {
		orientation.y = -1;
	}

	if (angleZ < 45) {
		orientation.z = 1;
	}
	else if (angleZ > 135) {
		orientation.z = -1;
	}
	return orientation;
}

/// <summary>
/// Returns the angle between 2 vectors. In radians.
/// </summary>
/// <param name="a">First vector.</param>
/// <param name="b">Second vector.</param>
/// <returns></returns>
float inline MathHelper::angleBetween(glm::vec3 a, glm::vec3 b) {
	/*glm::vec3 nA = glm::normalize(a);
	glm::vec3 nB = glm::normalize(b);
	return acos(glm::dot(nA, nB));*/

	return acos(glm::dot(glm::normalize(a), glm::normalize(b)));
}

float MathHelper::angleBetweenNormal(glm::vec3 a, glm::vec3 b, glm::vec3 n) {
	a = glm::normalize(a);
	b = glm::normalize(b);
	n = glm::normalize(n);

	float dot = a.x*b.x + a.y*b.y + a.z*b.z;
	float det = a.x*b.y*n.z + b.x*n.y*a.z + n.x*a.y*b.z - a.z*b.y*n.x - b.z*n.y*a.x - n.z*a.y*b.x;
	float angle = atan2(det, dot);
	return angle;
}



/// <summary>
/// Returns the quadrant that a vector is pointing to
/// </summary>
/// <param name="disp">Vector</param>
/// <returns></returns>
glm::ivec3 MathHelper::getVectorQuadrant(glm::vec3 disp) {

	glm::ivec3 orientation(0, 0, 0);

	if (disp.x != 0 && disp.y != 0 && disp.z != 0) {
		orientation.x = disp.x < 0 ? -1 : 1;
		orientation.y = disp.y < 0 ? -1 : 1;
		orientation.z = disp.z < 0 ? -1 : 1;
	}

	return orientation;
}

/// <summary>
/// ------------------ NOT WORKING ---------------------
/// Returns de quadrant that a vector is pointing. The quadrants are defined by the 3 axis given
/// </summary>
/// <param name="disp">The disp.</param>
/// <param name="axisX">The axis x.</param>
/// <param name="axisY">The axis y.</param>
/// <param name="axisZ">The axis z.</param>
/// <returns></returns>
glm::ivec3 MathHelper::getVectorQuadrantCustomAxis(glm::vec3 disp, glm::vec3 axisX, glm::vec3 axisY, glm::vec3 axisZ) {

	glm::ivec3 orientation;

	double angleX = glm::degrees(angleBetween(disp, axisX));
	double angleY = glm::degrees(angleBetween(disp, axisY));
	double angleZ = glm::degrees(angleBetween(disp, axisZ));

	if (angleX < 45) {
		orientation.x = 1;
	}
	else if (angleX > 135) {
		orientation.x = -1;
	}

	if (angleY < 45) {
		orientation.y = 1;
	}
	else if (angleY > 135) {
		orientation.y = -1;
	}

	if (angleZ < 45) {
		orientation.z = 1;
	}
	else if (angleZ > 135) {
		orientation.z = -1;
	}
	return orientation;
}


/// <summary>
/// Returns the vector C->D where D belongs to the line A->B and the distance from C to D is the minimum
/// </summary>
/// <param name="A">A: First point of the line</param>
/// <param name="B">B: Second point of the line</param>
/// <param name="C">C: Point to look for closest</param>
/// <returns></returns>
glm::vec3 MathHelper::shortestVecToLine(glm::vec3 vp1, glm::vec3 vp2, glm::vec3 vl, glm::vec3 pp, glm::vec3 lp) {

	float c; //see notes

			 //to get the point only c is needed
	c = (lp.z - pp.z - (lp.x *vp1.z) / vp1.x + (pp.x* vp1.z) / vp1.x + (lp.y* vp1.z* vp2.x) / (-vp1.y *vp2.x + vp1.x *vp2.y) - (pp.y *vp1.z *vp2.x) / (-vp1.y *vp2.x + vp1.x* vp2.y) - (lp.x* vp1.y* vp1.z* vp2.x) / (vp1.x*(-vp1.y *vp2.x + vp1.x* vp2.y)) + (pp.x* vp1.y* vp1.z* vp2.x) / (vp1.x*(-vp1.y *vp2.x + vp1.x* vp2.y)) - (lp.y *vp1.x* vp2.z) / (-vp1.y* vp2.x + vp1.x* vp2.y) + (pp.y* vp1.x* vp2.z) / (-vp1.y* vp2.x + vp1.x* vp2.y) + (lp.x* vp1.y *vp2.z) / (-vp1.y *vp2.x + vp1.x *vp2.y) - (pp.x *vp1.y *vp2.z) / (-vp1.y* vp2.x + vp1.x* vp2.y)) / (-vl.z + (vl.x* vp1.z) / vp1.x - (vl.y* vp1.z* vp2.x) / (-vp1.y* vp2.x + vp1.x *vp2.y) + (vl.x *vp1.y* vp1.z* vp2.x) / (vp1.x*(-vp1.y* vp2.x + vp1.x *vp2.y)) + (vl.y* vp1.x* vp2.z) / (-vp1.y* vp2.x + vp1.x* vp2.y) - (vl.x *vp1.y *vp2.z) / (-vp1.y *vp2.x + vp1.x *vp2.y));
	//b = (lp.y *vp1.x - pp.y* vp1.x + c* vl.y* vp1.x - lp.x* vp1.y + pp.x *vp1.y - c* vl.x *vp1.y) / (-vp1.y* vp2.x + vp1.x* vp2.y);
	//a = (lp.x - pp.x + c *vl.x - b* vp2.x) / vp1.x;

	glm::vec3 Q;

	Q = lp + c * vl;

	return Q;
}

/// <summary>
/// Changes the reference system from the absolute to the given.
/// </summary>
/// <param name="point">The point to change (absolute coordinates).</param>
/// <param name="newOrigin">The new origin.</param>
/// <param name="newX">The new x axis.</param>
/// <param name="newY">The new y axis.</param>
/// <param name="newZ">The new z axis.</param>
/// <returns>A vector containig the changed point</returns>
glm::vec3 MathHelper::changeReferenceSystem(glm::vec3 point, glm::vec3 newOrigin, glm::vec3 newX, glm::vec3 newY, glm::vec3 newZ) {

	point = point - newOrigin;
	glm::vec3 newPoint;

	newPoint.z = (-((newX.z* point.x) / newX.x) - (newX.y* newX.z* newY.x* point.x) / (newX.x*(-newX.y *newY.x + newX.x *newY.y)) + (newX.y *newY.z *point.x) / (-newX.y* newY.x + newX.x* newY.y) + (newX.z *newY.x *point.y) / (-newX.y *newY.x + newX.x *newY.y) - (newX.x *newY.z *point.y) / (-newX.y* newY.x + newX.x *newY.y) + point.z) / (-((newX.z *newZ.x) / newX.x) - (newX.y* newX.z *newY.x *newZ.x) / (newX.x*(-newX.y* newY.x + newX.x *newY.y)) + (newX.y* newY.z *newZ.x) / (-newX.y* newY.x + newX.x *newY.y) + (newX.z* newY.x *newZ.y) / (-newX.y* newY.x + newX.x *newY.y) - (newX.x* newY.z* newZ.y) / (-newX.y *newY.x + newX.x *newY.y) + newZ.z);
	newPoint.y = (newPoint.z* newX.y *newZ.x - newPoint.z* newX.x* newZ.y - newX.y *point.x + newX.x* point.y) / (-newX.y *newY.x + newX.x* newY.y);
	newPoint.x = (-newPoint.y* newY.x - newPoint.z* newZ.x + point.x) / newX.x;

	return newPoint;
}

glm::vec3 MathHelper::leaveReferenceSystem(glm::vec3 point, glm::vec3 origin, glm::vec3 vecX, glm::vec3 vecY, glm::vec3 vecZ) {

	return origin + point.x * vecX + point.y * vecY + point.z * vecZ;

}

glm::vec3 MathHelper::rotatePointArroundAxis(glm::vec3 point, glm::vec3 axis, double angle) {

	float v1 = point.x * axis.x + point.y * axis.y + point.z * axis.z;
	double oneMinusCos = 1 - cos(angle);
	double axisModule = sqrt(axis.x * axis.x + axis.y * axis.y + axis.z * axis.z);
	float axisM = axis.x * axis.x + axis.y * axis.y + axis.z * axis.z;

	glm::vec3 rotatedPoint;

	rotatedPoint.x = (float)((axis.x * v1 * oneMinusCos + axisM * point.x * cos(angle) + axisModule * (-axis.z * point.y + axis.y * point.z) * sin(angle)) / axisM);
	rotatedPoint.y = (float)((axis.y * v1 * oneMinusCos + axisM * point.y * cos(angle) + axisModule * (axis.z * point.x - axis.x * point.z) * sin(angle)) / axisM);
	rotatedPoint.z = (float)((axis.z * v1 * oneMinusCos + axisM * point.z * cos(angle) + axisModule * (-axis.y * point.x + axis.x * point.y) * sin(angle)) / axisM);

	return rotatedPoint;
}

/// <summary>
/// Gets the angles from aim.
/// </summary>
/// <param name="viewAxis">The view axis.</param>
/// <returns>"std::pair-pitch, yaw- in radians"</returns>
std::pair<double, double> MathHelper::getAnglesFromAim(glm::vec3 viewAxis) {

	glm::normalize(viewAxis);

	double pitch = atan2(viewAxis.y, sqrt(viewAxis.x * viewAxis.x + viewAxis.z * viewAxis.z));
	double yaw = -atan2(viewAxis.z, viewAxis.x);

	std::pair<double, double> returnValues;
	returnValues.first = pitch;
	returnValues.second = yaw;
	return returnValues;
}

//https://www.maplesoft.com/support/help/Maple/view.aspx?path=MathApps/ProjectionOfVectorOntoPlane
double MathHelper::findRoll(glm::vec3 axisUp, glm::vec3 axisRight) {

	double roll = MathHelper::angleBetween(axisRight, glm::vec3(axisRight.z, 0, axisRight.x));

	if (axisRight.y < 0) {
		roll = 2 * PI - roll;
	}
	return roll;

}


/// <summary>
/// Determines whether the given point is inside the paralelogram formed by parStart and parEnd. Remembrer, this paralelogram vertices are paralel to the world axis (1,0,0)(0,1,0)(0,0,1).
/// </summary>
/// <param name="point">The point.</param>
/// <param name="parStart">Paralelogram start.</param>
/// <param name="parEnd">Paralelogram end.</param>
/// <returns></returns>
bool MathHelper::isInsideParalelogram(glm::vec3 point, glm::vec3 parStart, glm::vec3 parEnd)
{

	glm::vec3 parDiagonal = parEnd - parStart;
	glm::vec3 pointDiagonal = point - parStart;

	bool inside = true;

	if (parDiagonal.x > 0) {
		if (pointDiagonal.x < 0 || pointDiagonal.x > parDiagonal.x) {
			inside = false;
		}
	}
	else {
		if (pointDiagonal.x > 0 || pointDiagonal.x < parDiagonal.x) {
			inside = false;
		}
	}

	if (parDiagonal.y > 0) {
		if (pointDiagonal.y < 0 || pointDiagonal.y > parDiagonal.y) {
			inside = false;
		}
	}
	else {
		if (pointDiagonal.y > 0 || pointDiagonal.y < parDiagonal.y) {
			inside = false;
		}
	}

	if (parDiagonal.z > 0) {
		if (pointDiagonal.z < 0 || pointDiagonal.z > parDiagonal.z) {
			inside = false;
		}
	}
	else {
		if (pointDiagonal.z > 0 || pointDiagonal.z < parDiagonal.z) {
			inside = false;
		}
	}

	return inside;
}


glm::vec3 MathHelper::findVectorPointToParalelogram(glm::vec3 point, glm::vec3 polStart, glm::vec3 polEnd)
{
	glm::vec3 result(0, 0, 0);

	/*glm::vec3 polDiagonal = polEnd - polStart;
	glm::vec3 polStartToPoint = point - polStart;

	if (polDiagonal.x > 0) {
	if (polStartToPoint.x < 0) {
	result.x = polStart.x - point.x;
	}
	else if (polStartToPoint.x > polDiagonal.x) {
	result.x = polEnd.x - point.x;
	}
	}
	else {
	if (polStartToPoint.x > 0) {
	result.x = polStart.x - point.x;
	}
	else if (polStartToPoint.x < polDiagonal.x) {
	result.x = polEnd.x - point.x;
	}
	}

	if (polDiagonal.y > 0) {
	if (polStartToPoint.y < 0) {
	result.y = polStart.y - point.y;
	}
	else if (polStartToPoint.y > polDiagonal.y) {
	result.y = polEnd.y - point.y;
	}
	}
	else {
	if (polStartToPoint.y > 0) {
	result.y = polStart.y - point.y;
	}
	else if (polStartToPoint.y < polDiagonal.y) {
	result.y = polEnd.y - point.y;
	}
	}

	if (polDiagonal.z > 0) {
	if (polStartToPoint.z < 0) {
	result.z = polStart.z - point.z;
	}
	else if (polStartToPoint.z > polDiagonal.z) {
	result.z = polEnd.z - point.z;
	}
	}
	else {
	if (polStartToPoint.z > 0) {
	result.z = polStart.z - point.z;
	}
	else if (polStartToPoint.z < polDiagonal.z) {
	result.z = polEnd.z - point.z;
	}
	}*/

	return result;
}

glm::vec3 MathHelper::getClosestPointToPolygon(glm::vec3 polStart, glm::vec3 polEnd, glm::vec3 relPosition, glm::vec3 relFront) {

	glm::vec3 sidesToCheck(0, 0, 0), finalValue(0, 0, 0);
	bool done = false;

	sidesToCheck.x = (relFront.x < 0) ? polEnd.x : polStart.x;
	sidesToCheck.y = (relFront.y < 0) ? polEnd.y : polStart.y;
	sidesToCheck.z = (relFront.z < 0) ? polEnd.z : polStart.z;

	/*
	Line equation:
	x = Px + k*Vx;
	y = Py + k*Vy;
	z = Pz + k*Vz;
	----------------------------------
	given: x = a; "a" is a known value

	k = (a - Px) / Vx;

	y = Py + ((a - P2) / Vx) * Vy;
	z = Pz + ((a - P2) / Vx) * Vz;
	*/

	float k = (sidesToCheck.x - relPosition.x) / relFront.x;

	if (k > 0) { //We only want points in front of the player, so K must be bigger than 0
		float y = relPosition.y + k * relFront.y;

		if (y > polStart.y && y < polEnd.y) { //If the Y we found is inside the section of the plane that contains the polygon we will look for the Z
			float z = relPosition.z + k * relFront.z;

			if (z > polStart.z && z < polEnd.z) {	//If Z is also in the polygon, we are done.
				finalValue = glm::vec3(sidesToCheck.x, y, z);
				done = true;
			}
		}
	}


	if (!done) {	//if we didn't find the point yet, we look in the next side.
		float k = (sidesToCheck.y - relPosition.y) / relFront.y;
		if (k > 0) {
			float x = relPosition.x + k * relFront.x;

			if (x > polStart.x && x < polEnd.x) {
				float z = relPosition.z + k * relFront.z;
				if (z > polStart.z && z < polEnd.z) {
					finalValue = glm::vec3(x, sidesToCheck.y, z);
					done = true;
				}
			}
		}
	}

	if (!done) {

		float k = (sidesToCheck.z - relPosition.z) / relFront.z;
		if (k > 0) {
			float x = relPosition.x + k * relFront.x;

			if (x > polStart.x && x < polEnd.x) {
				float y = relPosition.y + k * relFront.y;
				if (y > polStart.y && y < polEnd.y) {
					finalValue = glm::vec3(x, y, sidesToCheck.z);
					done = true;
				}
			}
		}
	}

	return finalValue;
}

glm::vec3 MathHelper::snapPoint(glm::vec3 pointToSnap, float gridSize)
{
	float divX = round(pointToSnap.x / gridSize);
	float divY = round(pointToSnap.y / gridSize);
	float divZ = round(pointToSnap.z / gridSize);

	glm::vec3 snappedPoint(divX * gridSize, divY * gridSize, divZ * gridSize);

	return snappedPoint;
}

/// <summary>
/// Calculates the distance between a point and a plane.
/// Returns a negative value if the point is behind the plane.
/// USES VECTOR EQUATION http://mathinsight.org/distance_point_plane
/// </summary>
/// <param name="P">The Point.</param>
/// <param name="A">Parameter A in the vector equation.</param>
/// <param name="B">Parameter B in the vector equation.</param>
/// <param name="C">Parameter C in the vector equation.</param>
/// <param name="D">Parameter D in the vector equation.</param>
/// <returns>The distance to the plane, negative if the point is behind the plane</returns>
float MathHelper::distancePointToPlane(glm::vec3 P, float A, float B, float C, float D)
{
	float tmp1 = A * P.x + B * P.y + C * P.z + D;
	float tmp2 = sqrt(pow(A, 2) + pow(B, 2) + pow(C, 2));
	return tmp1 / tmp2;
}


/*
	
	CODI EXTRET DE http://geomalgorithms.com/a12-_hull-3.html
	EL DIA 2019-04-05
	
*/
// Copyright 2001 softSurfer, 2012 Dan Sunday
// This code may be freely used and modified for any purpose
// providing that this copyright notice is included with it.
// SoftSurfer makes no warranty for this code, and cannot be held
// liable for any real or imagined damage resulting from its use.
// Users of this code must verify correctness for their application.


// Assume that a class is already given for the object:
//    Point with coordinates {float x, y;}
//===================================================================


// isLeft(): test if a point is Left|On|Right of an infinite line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 and P1
//            =0 for P2 on the line
//            <0 for P2 right of the line
//    See: Algorithm 1 on Area of Triangles
float MathHelper::isLeft(glm::vec2 P0, glm::vec2 P1, glm::vec2 P2)
{
	return (P1.x - P0.x)*(P2.y - P0.y) - (P2.x - P0.x)*(P1.y - P0.y);
}



// simpleHull_2D(): Melkman's 2D simple polyline O(n) convex hull algorithm
//    Input:  P[] = array of 2D vertex points for a simple polyline
//            n   = the number of points in V[]
//    Output: H[] = output convex hull array of vertices (max is n)
//    Return: h   = the number of points in H[]
int MathHelper::simpleHull_2D(std::vector<glm::vec2> * P, std::vector<glm::vec2> * H)
{
	int n = P->size();

	// initialize a deque D->at(] from bottom to top so that the
	// 1st three vertices of P->at(])are a ccw triangle
	std::vector<glm::vec2> * D = new std::vector<glm::vec2>;

	int bot = n - 2, top = bot + 3;    // initial bottom and top deque indices
	
	for (int i = 0; i < top + 1; i++)
	{
		D->push_back(glm::vec2(0, 0));
	}

	D->at(bot) = P->at(2);        // 3rd vertex is at both bot and top
	D->at(top) = P->at(2);
	if (MathHelper::isLeft(P->at(0), P->at(1), P->at(2)) > 0) {
		D->at(bot + 1) = P->at(0);
		D->at(bot + 2) = P->at(1);           // ccw vertices are: 2,0,1,2
	}
	else {
		D->at(bot + 1) = P->at(1);
		D->at(bot + 2) = P->at(0);           // ccw vertices are: 2,1,0,2
	}

	// compute the hull on the deque D->at(]
	for (int i = 3; i < n; i++) {   // process the rest of vertices
									// test if next vertex is inside the deque hull
		if ((MathHelper::isLeft(D->at(bot), D->at(bot + 1), P->at(i)) > 0) &&
			(MathHelper::isLeft(D->at(top - 1), D->at(top), P->at(i)) > 0))
			continue;         // skip an interior vertex

							  // incrementally add an exterior vertex to the deque hull
							  // get the rightmost tangent at the deque bot
		while (MathHelper::isLeft(D->at(bot), D->at(bot + 1), P->at(i)) <= 0)
			++bot;                 // remove bot of deque
		D->at(--bot) = P->at(i);           // insert P->at(i) at bot of deque

										   // get the leftmost tangent at the deque top
		while (MathHelper::isLeft(D->at(top - 1), D->at(top), P->at(i)) <= 0)
			--top;                 // pop top of deque
		D->at(++top) = P->at(i);           // push P->at(i) onto top of deque
	}

	for (int i = 0; i < (top - bot)+1; i++)
	{
		H->push_back(glm::vec2(0, 0));
	}
	// transcribe deque D->at(] to the output hull array H[]
	int h;        // hull vertex counter
	for (h = 0; h <= (top - bot); h++)
		H->at(h) = D->at(bot + h);

	delete D;
	return h - 1;
}

//UTILS
vector<string> MathHelper::explode(const string& s, const char& c)
{
	string buff{ "" };
	vector<string> v;

	for (auto n : s)
	{
		if (n != c) buff += n; else
			if (n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if (buff != "") v.push_back(buff);

	return v;
}