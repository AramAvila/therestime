#pragma once
#include <iostream>

#include "Physics.h"
#include "Player.h"
#include "Level.h"

class SceneManager
{
public:
	SceneManager();
	~SceneManager();



	Player active_player;
	float delta_time;

	glm::vec3 player_order = glm::vec3(0, 0, 0);

	Level current_level;

	glm::vec2 gravity = glm::vec2(0, -9.81);
	float move_speed = 0.1;
	float jump_strength = 10;
	
	int timeline_length = 300;
	
	//Time elapsed in each insant, in seconds
	float instant_length = 0.05;
	float instant_thickness = 0.1;
	float instant_to_update = -1;


	bool player_completed = false;
	bool flatter_completed = false;
	int instant_completed = -1;

	std::vector<Level> timeline;
	std::vector<ORDER_LIST> order_list;
	std::vector<ACTION_LIST> action_list;

	void SceneManager::processKeyInput(int key, int scancode, int action, int mode, float deltaTime);

	void SceneManager::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);

	void SceneManager::ProcessMouseScroll(float yoffset);

	void SceneManager::ProcessPlayerPhysics();

	void SceneManager::LoadLevel(std::string level_name);

	void SceneManager::UpdateTimeline();
	void SceneManager::UpdateInstant();

	Level SceneManager::CloneInstant(Level instant, float time);

	void SceneManager::ResetActions();
};