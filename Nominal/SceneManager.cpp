#include "SceneManager.h"



SceneManager::SceneManager()
{
	this->active_player.updateCameraVectors();
}


SceneManager::~SceneManager()
{
}


void SceneManager::processKeyInput(int key, int scancode, int action, int mode, float deltaTime) {

	float velocity = this->active_player.MovementSpeed * deltaTime;
	this->active_player.Velocity = velocity;
	int instant_index = floor(this->active_player.Position.z * 10.0f);
	bool the_order_is_given = false;

	switch (key)
	{
		case 81://Q
			if (instant_index >= 0 && instant_index < this->timeline_length) {
				this->order_list.at(instant_index) = ORDER_LIST::BACKWARDS;
				this->instant_to_update = instant_index;
				the_order_is_given = true;
			}
		
			break;

		case 69://E
			if (instant_index >= 0 && instant_index < this->timeline_length) {
				this->order_list.at(instant_index) = ORDER_LIST::FORWARDS;
				this->instant_to_update = instant_index;
				the_order_is_given = true;
			}
			break;

		case 70://F
			if (instant_index >= 0 && instant_index < this->timeline_length) {
				this->order_list.at(instant_index) = ORDER_LIST::STOP;
				this->action_list.at(instant_index) = ACTION_LIST::ACTION_NONE;
				this->instant_to_update = instant_index;
				the_order_is_given = true;
			}
			break;

		case 82://R
			if (instant_index >= 0 && instant_index < this->timeline_length) {
				this->action_list.at(instant_index) = ACTION_LIST::JUMP;
				this->instant_to_update = instant_index;
				the_order_is_given = true;
			}
			break;

		default:
			break;
	}

	
	if (the_order_is_given) {
		this->flatter_completed = false;
		for (int i = instant_index + 1; i < this->timeline_length; i++)
		{
			this->order_list.at(i) = ORDER_LIST::ORDER_NONE;
			this->action_list.at(i) = ACTION_LIST::ACTION_NONE;
		}
	}
	

};

void SceneManager::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	xoffset *= this->active_player.MouseSensitivity;
	yoffset *= this->active_player.MouseSensitivity;

	this->active_player.Pitch += yoffset;
	this->active_player.Yaw += xoffset;

	this->active_player.lastTickMove.x = xoffset;
	this->active_player.lastTickMove.y = yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (this->active_player.Pitch > 89.0f)
			this->active_player.Pitch = 89.0f;
		if (this->active_player.Pitch < -89.0f)
			this->active_player.Pitch = -89.0f;
	}

	// Update Front, Right and Up Vectors using the updated Euler angles
	this->active_player.updateCameraVectors();
}

// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
void SceneManager::ProcessMouseScroll(float yoffset)
{
	if (this->active_player.Zoom >= 1.0f && this->active_player.Zoom <= 45.0f)
		this->active_player.Zoom -= yoffset;
	if (this->active_player.Zoom <= 1.0f)
		this->active_player.Zoom = 1.0f;
	if (this->active_player.Zoom >= 45.0f)
		this->active_player.Zoom = 45.0f;
}

void SceneManager::ProcessPlayerPhysics()
{

	int instant_index = floor(this->active_player.Position.z * 10.0f);
	Level player_level_instant;

	if (instant_index >= 0 && instant_index < this->current_level.level_length) {
		player_level_instant = this->timeline.at(instant_index);
	}
	else {
		player_level_instant = this->timeline.at(0);
		player_level_instant.solid_list.clear();
		player_level_instant.platform_list.clear();
 		player_level_instant.deathSurface_list.clear();
	}

	//Calculem les fisiques i colisions del jugador i a la vegada comprovem que no hagi acabat el nivell.
	this->player_completed = Physics::ProcessPlayerInstant(&this->active_player, this->player_order, &player_level_instant, this->delta_time);

	//this->player_order = glm::vec3(0, 0, 0);
}

void SceneManager::LoadLevel(std::string level_name)
{
	this->current_level = * new Level(level_name);
	this->timeline_length = this->current_level.level_length;
	for (int i = 0; i < this->current_level.level_length; i++)
	{
		this->order_list.push_back(ORDER_LIST::ORDER_NONE);
		this->action_list.push_back(ACTION_LIST::ACTION_NONE);
	}

	this->ResetActions();
	this->timeline.clear();
}

void SceneManager::UpdateTimeline()
{

	ORDER_LIST last_order = STOP;
	ORDER_LIST order_to_process = STOP;
	
	this->flatter_completed = false;

	Level * last_level = new Level();

	last_level->absPosition = this->current_level.absPosition;
	last_level->VAO = this->current_level.VAO;
	last_level->VBO = this->current_level.VBO;
	last_level->name = this->current_level.name;
	last_level->vertexCount = this->current_level.vertexCount;
	//last_level->vertexValues = this->current_level.vertexValues;
	last_level->solid_list = this->current_level.solid_list;

	last_level->instant_length = this->instant_length;
	last_level->gravity = this->gravity;

	last_level->flatter_VAO = this->current_level.flatter_VAO;
	last_level->flatter_VBO = this->current_level.flatter_VBO;
	last_level->flatter_vertexCount = this->current_level.flatter_vertexCount;
	last_level->flatter_flatterOuterHull = this->current_level.flatter_flatterOuterHull;
	last_level->flatter_position = this->current_level.flatter_starting_position;
	last_level->flatter_momentum = this->current_level.flatter_momentum;
	last_level->flatter_center = this->current_level.flatter_center;

	last_level->distortion_init_list = this->current_level.distortion_init_list;
	last_level->distortion_end_list = this->current_level.distortion_end_list;

	last_level->goal_position = this->current_level.goal_position;
	last_level->platform_list = this->current_level.platform_list;
	last_level->deathSurface_list = this->current_level.deathSurface_list;

	last_level->arrow_VAO = this->current_level.arrow_VAO;
	last_level->arrow_VBO = this->current_level.arrow_VBO;
	last_level->arrow_vertexCount = this->current_level.arrow_vertexCount;
	
	last_level->level_text_list = this->current_level.level_text_list;

	if (this->timeline.size() == 0) { 
		this->timeline.push_back(*last_level);
	}
	else {
		this->timeline.at(0) = *last_level;
	}

	for (int i = 0; i < this->current_level.level_length; i++)
	{

		Level * instant;
		if (this->timeline.size() > i) {
			instant = &this->timeline.at(i);
		}
		else {
			instant = new Level();
		}

		instant->absPosition = this->current_level.absPosition + glm::vec3(0, 0, i * instant_thickness);

		instant->VAO = last_level->VAO;
		instant->VBO = last_level->VBO;
		instant->name = last_level->name;
		instant->solid_list = last_level->solid_list;
		instant->vertexCount = last_level->vertexCount;

		instant->flatter_VAO = last_level->flatter_VAO;
		instant->flatter_VBO = last_level->flatter_VBO;
		instant->flatter_vertexCount = last_level->flatter_vertexCount;
		instant->flatter_flatterOuterHull = last_level->flatter_flatterOuterHull;
		instant->flatter_position = last_level->flatter_position;
		instant->flatter_momentum = last_level->flatter_momentum;
		instant->flatter_center = last_level->flatter_center;

		instant->instant_length = this->instant_length;
		instant->gravity = this->gravity;

		instant->distortion_init_list = last_level->distortion_init_list;
		instant->distortion_end_list = last_level->distortion_end_list;

		instant->goal_position = last_level->goal_position;
		instant->platform_list = last_level->platform_list;
		instant->deathSurface_list = last_level->deathSurface_list;

		instant->arrow_VAO = last_level->arrow_VAO;
		instant->arrow_VBO = last_level->arrow_VBO;
		instant->arrow_vertexCount = last_level->arrow_vertexCount;

		instant->level_text_list = last_level->level_text_list;

		bool distort = false;
		for (int c = 0; c < instant->distortion_init_list.size(); c++)
		{
			if (i > instant->distortion_init_list.at(c) && i < instant->distortion_end_list.at(c)) {
				distort = true;
			}
		}

		if (!distort) {
			instant->action = this->action_list.at(i);
		}
		else {
			instant->action = ACTION_LIST::ACTION_NONE;
		}
		
		if (this->order_list.at(i) == ORDER_LIST::ORDER_NONE || distort) {
			order_to_process = last_order;
		}
		else {
			order_to_process = this->order_list.at(i);
			last_order = this->order_list.at(i);
		}

		instant->order = order_to_process;

		
		Physics::ProcessInstant(instant);

		if (instant->flatter_has_reached_end) {
			this->flatter_completed = true;
			this->instant_completed = i;
			i = this->current_level.level_length;
		}

		last_level = instant;
		
		if (this->timeline.size() <= i) {
			this->timeline.push_back(*instant);
		}
	}
}

void SceneManager::UpdateInstant() {

	int instant_index = this->instant_to_update;

	if (instant_index > 0 && instant_index < this->current_level.level_length - 1) {
		Level * last_level = &this->timeline.at(instant_to_update - 1);
		Level * current_instant = &this->timeline.at(instant_to_update);

		current_instant->flatter_position = last_level->flatter_position;
		current_instant->flatter_momentum = last_level->flatter_momentum;

		current_instant->platform_list = last_level->platform_list;
		current_instant->deathSurface_list = last_level->deathSurface_list;

		bool distort = false;
		for (int c = 0; c < current_instant->distortion_init_list.size(); c++)
		{
			if (instant_index > current_instant->distortion_init_list.at(c) && instant_index < current_instant->distortion_end_list.at(c)) {
				distort = true;
			}
		}

		ORDER_LIST order_definitive;

		if (distort) {
			current_instant->action = ACTION_LIST::ACTION_NONE;
		}
		else {
			current_instant->action = this->action_list.at(instant_index);;
		}

		if (this->order_list.at(instant_index) == ORDER_LIST::ORDER_NONE || distort) {
			order_definitive = last_level->order;
		}
		else {
			order_definitive = this->order_list.at(instant_index);
		}

		current_instant->order = order_definitive;

		Physics::ProcessInstant(current_instant);

		this->timeline.at(instant_index) = *current_instant;

		this->instant_to_update = ++instant_index;

		if (current_instant->flatter_has_reached_end) {
			this->flatter_completed = true;
			this->instant_completed = instant_index;
			this->instant_to_update = -1;
		}

	}
	else if(instant_index >= this->current_level.level_length){
		this->instant_to_update = - 1;
	}
	
}

void SceneManager::ResetActions()
{
	for (int i = 0; i < this->order_list.size(); i++)
	{
		this->order_list.at(i) = ORDER_LIST::ORDER_NONE;
		this->action_list.at(i) = ACTION_LIST::ACTION_NONE;
	}
}
