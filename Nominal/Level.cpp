#include "Level.h"

Level::Level()
{
}

Level::Level(std::string level_name)
{
	this->name = level_name;
	this->loadLevel(level_name);
	this->loadPlayer(this->flatter_file);
}

Level::~Level()
{
}

void Level::loadLevel(std::string path) {

	std::cout << "Loading level: " << path << std::endl;

	std::string line;

	std::string file_path = "levels/" + path;
	std::ifstream myfile(file_path);

	if (!myfile)
	{
		std::cout << "File not found " << file_path << std::endl;
	}

	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
		{
			//std::string a = MathHelper::explode(line, ',');

			std::string buff{ "" };
			std::vector<std::string> line_split;

			for (auto n : line)
			{
				if (n != ',') buff += n; else
					if (n == ',' && buff != "") { line_split.push_back(buff); buff = ""; }
			}

			if (buff != "") line_split.push_back(buff);
			/*player_position, 1, 1.5
				flatter_position, 1, 0.5
				goal_position, 8, 0.5
				level_length, 80
				distortion_list, 0, 10
				distortion_list, 15, 20
				platform_list
				level_obj, lvl1.obj
				*/

			if (line_split.at(0) == "player_position") {
				this->player_starting_position = glm::vec2(std::stof(line_split.at(1)), std::stof(line_split.at(2)));
			}

			if (line_split.at(0) == "flatter_position") {
				this->flatter_starting_position = glm::vec2(std::stof(line_split.at(1)), std::stof(line_split.at(2)));
			}

			if (line_split.at(0) == "goal_position") {
				this->goal_position = glm::vec2(std::stof(line_split.at(1)), std::stof(line_split.at(2)));
			}

			if (line_split.at(0) == "text") {
				this->level_text_list.push_back(std::make_pair(glm::vec3(std::stof(line_split.at(1)), std::stof(line_split.at(2)), std::stof(line_split.at(3))), line_split.at(4)));
			}

			if (line_split.at(0) == "level_length") {
				this->level_length = std::stoi(line_split.at(1));
			}

			if (line_split.at(0) == "distortion_list") {
				this->distortion_init_list.push_back(std::stoi(line_split.at(1)));
				this->distortion_end_list.push_back(std::stoi(line_split.at(2)));
			}

			if (line_split.at(0) == "level_obj") {
				this->level_file = line_split.at(1);
			}
			
			if (line_split.at(0) == "player_fall_reset_threshold") {
				this->player_fall_reset_threshold = std::stof(line_split.at(1));
			}

			if (line_split.at(0) == "platform") {
				Platform * pl = new Platform();
				pl->model_name = line_split.at(1);
				pl->init_position = glm::vec2(std::stof(line_split.at(2)), std::stof(line_split.at(3)));
				pl->current_position = glm::vec2(std::stof(line_split.at(2)), std::stof(line_split.at(3)));
				pl->end_position = glm::vec2(std::stof(line_split.at(4)), std::stof(line_split.at(5)));
				pl->speed = std::stof(line_split.at(6));

				this->loadObjectModel(pl->model_name, &pl->solid_list, pl->modelVertices, &pl->vertexCount, &pl->vertexValues);

				this->platform_list.push_back(*pl);
			}

			if (line_split.at(0) == "death_surface") {
				DeathSurface * ds = new DeathSurface();
				ds->model_name = line_split.at(1);

				this->loadObjectModel(ds->model_name, &ds->solid_list, ds->modelVertices, &ds->vertexCount, &ds->vertexValues);

				this->deathSurface_list.push_back(*ds);
			}

		}
		myfile.close();

		if (this->level_file.size() > 0) {
			this->loadObjectModel(this->level_file, &this->solid_list, this->modelVertices, &this->vertexCount, &this->vertexValues);
		}
		else {
			throw std::invalid_argument("Level object file not found.");
		}

		this->loadObjectModel("arrow.obj", NULL, this->arrow_modelVertices, &this->arrow_vertexCount, &this->arrow_vertexValues);
	}

}


void Level::loadObjectModel(std::string path, std::vector<Triangle> * local_solid_list, float * local_model_vertices, int * local_vertex_count, std::vector<float> * local_vertexValues) {

	std::cout << "Loading level model: " << path << std::endl;

	struct dataPoint {
		float x;
		float y;
		float z;
	};


	std::string line;

	std::string file_path = "levels/" + path;
	std::ifstream myfile(file_path);

	if (!myfile)
	{
		throw "File not found " + file_path;
	}

	std::vector<dataPoint> vertices;
	std::vector<dataPoint> normals;
	std::vector<dataPoint> textures;
	std::vector<dataPoint> vertexComposition;

	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
		{
			std::string a = line.substr(0, 2);
			std::string::iterator it = line.begin();

			if (a == "v ") {

				std::string num = line;
				std::string val;
				float x, y, z;

				int	split = num.find(" "); // num = v 0.5 -0.5 -0.5
				num.erase(0, split + 1);	   // num = 0.5 -0.5 -0.5

				split = num.find(" ");
				val = num.substr(0, split);
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0.5 -0.5 -0.5, x = 0.5
				num.erase(0, split + 1);			// num = -0.5 -0.5

				split = num.find(" ");
				val = num.substr(0, split);
				y = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = -0.5 -0.5, y = -0.5
				num.erase(0, split + 1);			// num = -0.5

				z = num.find("e") != std::string::npos ? 0 : std::stof(num); // num = -0.5, z = -0.5

				dataPoint point;
				point.x = x;
				point.y = y;
				point.z = z;
				vertices.push_back(point);
			}

			else if (a == "vt") {
				std::string num = line;
				std::string val;
				float x, y;

				int	split = num.find(" "); // num = vt -1 -1
				num.erase(0, split + 1);	   // num = -1 -1

				split = num.find(" ");
				val = num.substr(0, split);			// num = -1 -1, val = -1
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); //some coordinates have the scientific notation (10e-18)
				num.erase(0, split + 1);			// num = -1

				y = num.find("e") != std::string::npos ? 0 : std::stof(num); // num = -0.5 -0.5, y = -0.5

				dataPoint point;
				point.x = x;
				point.y = 1 - y;
				textures.push_back(point);
			}
			else if (a == "vn") {

				std::string num = line;
				std::string val;
				float x, y, z;

				int	split = num.find(" "); // num = vn 0 0 -1
				num.erase(0, split + 1);	   // num = 0 0 -1

				split = num.find(" ");
				val = num.substr(0, split);
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0 0 -1, x = 0
				num.erase(0, split + 1);			// num = 0 -1

				split = num.find(" ");
				val = num.substr(0, split);
				y = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0 -1, y = 0
				num.erase(0, split + 1);			// num = -1

				z = num.find("e") != std::string::npos ? 0 : std::stof(num);; // num = -1, z = -1

				dataPoint point;
				point.x = x;
				point.y = y;
				point.z = z;
				normals.push_back(point);
			}
			else if (a == "f ") {
				std::string num = line; // num = f 1/9/3 7/8/3 6/10/3
				int v, n, t;

				int split = num.find(" ");// num = f|1/9/3 7/8/3 6/10/3
				num.erase(0, split + 1);	 // num = 1/9/3 7/8/3 6/10/3

				std::string faces[3];

				for (int a = 0; a < 2; a++)
				{
					split = num.find(" ");
					faces[a] = num.substr(0, split);
					num.erase(0, split + 1);
				}
				faces[2] = num;

				for (int b = 0; b < 3; b++)
				{
					split = faces[b].find("/");
					v = std::stoi(faces[b].substr(0, split));
					faces[b].erase(0, split + 1);

					split = faces[b].find("/");
					t = std::stoi(faces[b].substr(0, split));
					faces[b].erase(0, split + 1);

					n = std::stoi(faces[b]);

					dataPoint point;
					point.x = v;
					point.y = n;
					point.z = t;
					vertexComposition.push_back(point);
				}
			}
		}
		myfile.close();
	}

	//GLsizeiptr size = vertexComposition.size() * sizeof(GLfloat); // number of vertices * those vertices are floats

	int c = 0;
	int v, n, t;
	Triangle tr = * new Triangle();
	float p1_z, p2_z, p3_z = INFINITY;
	int p1_v, p2_v, p3_v;
	for (int i = 0; i < vertexComposition.size(); i++)
	{
		
		v = vertexComposition[i].x - 1;
		local_model_vertices[c++] = vertices[v].x;
		local_model_vertices[c++] = vertices[v].y;
		local_model_vertices[c++] = vertices[v].z;

		n = vertexComposition[i].y - 1;
		local_model_vertices[c++] = normals[n].x;
		local_model_vertices[c++] = normals[n].y;
		local_model_vertices[c++] = normals[n].z;

		t = vertexComposition[i].z - 1;
		local_model_vertices[c++] = textures[t].x;
		local_model_vertices[c++] = textures[t].y;

		if (tr.point_1.x == INFINITY) {
			tr.point_1.x = vertices[v].x;
			tr.point_1.y = vertices[v].y;
			p1_z = vertices[v].z;
			p1_v = v;
		}
		else if(tr.point_2.x == INFINITY) {
			tr.point_2.x = vertices[v].x;
			tr.point_2.y = vertices[v].y;
			p2_z = vertices[v].z;
			p2_v = v;
		}
		else if (tr.point_3.x == INFINITY) {
			tr.point_3.x = vertices[v].x;
			tr.point_3.y = vertices[v].y;
			p3_z = vertices[v].z;
			p3_v = v;


			if (p1_z == 0 && p2_z == 0 && p3_z == 0) {
				if (local_solid_list != NULL) {
					local_solid_list->push_back(tr);
				}
				p1_z, p2_z, p3_z = INFINITY;
			}
			tr = *new Triangle();
		}
		
	}

	for (int i = 0; i < c; i++)
	{
		local_vertexValues->push_back(modelVertices[i]);
	}

	*local_vertex_count = vertexComposition.size();
}

void Level::loadPlayer(std::string path) {

	if (path.length() == 0) {
		path = this->flatter_file;
	}

	std::cout << "Loading flatter model: " << path << std::endl;

	struct dataPoint {
		float x;
		float y;
		float z;
	};


	std::string line;

	std::string file_path = "levels/" + path;
	std::ifstream myfile(file_path);

	if (!myfile)
	{
		std::cout << "File not found " << file_path << std::endl;
	}

	std::vector<dataPoint> vertices;
	std::vector<dataPoint> normals;
	std::vector<dataPoint> textures;
	std::vector<dataPoint> vertexComposition;

	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
		{
			std::string a = line.substr(0, 2);
			std::string::iterator it = line.begin();

			if (a == "v ") {

				std::string num = line;
				std::string val;
				float x, y, z;

				int	split = num.find(" "); // num = v 0.5 -0.5 -0.5
				num.erase(0, split + 1);	   // num = 0.5 -0.5 -0.5

				split = num.find(" ");
				val = num.substr(0, split);
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0.5 -0.5 -0.5, x = 0.5
				num.erase(0, split + 1);			// num = -0.5 -0.5

				split = num.find(" ");
				val = num.substr(0, split);
				y = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = -0.5 -0.5, y = -0.5
				num.erase(0, split + 1);			// num = -0.5

				z = num.find("e") != std::string::npos ? 0 : std::stof(num);; // num = -0.5, z = -0.5

				dataPoint point;
				point.x = x;
				point.y = y;
				point.z = z;
				vertices.push_back(point);
			}

			else if (a == "vt") {
				std::string num = line;
				std::string val;
				float x, y;

				int	split = num.find(" "); // num = vt -1 -1
				num.erase(0, split + 1);	   // num = -1 -1

				split = num.find(" ");
				val = num.substr(0, split);			// num = -1 -1, val = -1
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); //some coordinates have the scientific notation (10e-18)
				num.erase(0, split + 1);			// num = -1

				y = num.find("e") != std::string::npos ? 0 : std::stof(num); // num = -0.5 -0.5, y = -0.5

				dataPoint point;
				point.x = x;
				point.y = 1 - y;
				textures.push_back(point);
			}
			else if (a == "vn") {

				std::string num = line;
				std::string val;
				float x, y, z;

				int	split = num.find(" "); // num = vn 0 0 -1
				num.erase(0, split + 1);	   // num = 0 0 -1

				split = num.find(" ");
				val = num.substr(0, split);
				x = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0 0 -1, x = 0
				num.erase(0, split + 1);			// num = 0 -1

				split = num.find(" ");
				val = num.substr(0, split);
				y = val.find("e") != std::string::npos ? 0 : std::stof(val); // num = 0 -1, y = 0
				num.erase(0, split + 1);			// num = -1

				z = num.find("e") != std::string::npos ? 0 : std::stof(num);; // num = -1, z = -1

				dataPoint point;
				point.x = x;
				point.y = y;
				point.z = z;
				normals.push_back(point);
			}
			else if (a == "f ") {
				std::string num = line; // num = f 1/9/3 7/8/3 6/10/3
				int v, n, t;

				int split = num.find(" ");// num = f|1/9/3 7/8/3 6/10/3
				num.erase(0, split + 1);	 // num = 1/9/3 7/8/3 6/10/3

				std::string faces[3];

				for (int a = 0; a < 2; a++)
				{
					split = num.find(" ");
					faces[a] = num.substr(0, split);
					num.erase(0, split + 1);
				}
				faces[2] = num;

				for (int b = 0; b < 3; b++)
				{
					split = faces[b].find("/");
					v = std::stoi(faces[b].substr(0, split));
					faces[b].erase(0, split + 1);

					split = faces[b].find("/");
					t = std::stoi(faces[b].substr(0, split));
					faces[b].erase(0, split + 1);

					n = std::stoi(faces[b]);

					dataPoint point;
					point.x = v;
					point.y = n;
					point.z = t;
					vertexComposition.push_back(point);
				}
			}
		}
		myfile.close();
	}

	//GLsizeiptr size = vertexComposition.size() * sizeof(GLfloat); // number of vertices * those vertices are floats

	int c = 0;
	int v, n, t;

	std::vector<glm::vec2> point_list;

	for (int i = 0; i < vertexComposition.size(); i++)
	{
		v = vertexComposition[i].x - 1;
		flatter_modelVertices[c++] = vertices[v].x;
		flatter_modelVertices[c++] = vertices[v].y;
		flatter_modelVertices[c++] = vertices[v].z;

		n = vertexComposition[i].y - 1;
		flatter_modelVertices[c++] = normals[n].x;
		flatter_modelVertices[c++] = normals[n].y;
		flatter_modelVertices[c++] = normals[n].z;

		t = vertexComposition[i].z - 1;
		flatter_modelVertices[c++] = textures[t].x;
		flatter_modelVertices[c++] = textures[t].y;

		if (normals[n].x == 0 && normals[n].y == 0 && normals[n].z == 1) {
			point_list.push_back(glm::vec2(vertices[v].x, vertices[v].y));
		}
	}

	for (int i = 0; i < c; i++)
	{
		this->flatter_vertexValues.push_back(modelVertices[i]);
	}

	std::vector<glm::vec2> hull;

	MathHelper::simpleHull_2D(&point_list, &hull);

	this->flatter_flatterOuterHull = hull;

	glm::vec2 avg(0,0);

	for (std::vector<glm::vec2>::iterator point = hull.begin(); point != hull.end(); point++) {
		avg.x += point->x;
		avg.y += point->y;
	}

	this->flatter_center = glm::vec2(avg.x / hull.size(), avg.y / hull.size());

	this->flatter_vertexCount = vertexComposition.size();
}
