#pragma once
#include <map>
#include <vector>
#include <memory>

#include "gl\glew.h"
#include "gl\glm\vec3.hpp"
#include "gl\glm\vec2.hpp"

class Hud
{
public:
	Hud();
	~Hud();

	GLuint texturePack;
	std::map<char, std::vector<GLfloat>> alphabet;

	float CARACTER_KERNING = 0.15f,
		HUD_PRECISION = 2.0f, //Number of decimals the numbers huds will have
		CHARACTER_HEIGHT = 0.3f,
		CHARACTER_WIDTH = 0.15f,
		LINE_SPACING = 0.4f;

	glm::vec3 relativePos,
		relativeX,
		relativeY,
		relativeZ;

	GLuint hud_VAO, hud_VBO, hud_vertex_num;

	void addToHUD(glm::vec2 pos, float value);

	std::vector<std::pair<glm::vec2, std::vector<GLfloat>>> charArray;
};

