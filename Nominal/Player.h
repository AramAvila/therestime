#pragma once
#include <iostream>
#include <vector>

#include "gl\glm\vec3.hpp"
#include "gl/glew.h"
#include "gl/glm/glm.hpp"
#include "gl/glm/gtc/matrix_transform.hpp"
#include "MathHelper.h"

const float YAW = 90.0f;
const float PITCH = 10.0f;
const float SPEED = 3.0f;
const float SENSITIVITY = 0.1f;
const float ZOOM = 45.0f;

class Player
{
public:

	~Player();

		// Camera Attributes
		glm::vec3 Position;
		glm::vec3 Momentum = glm::vec3(0, 0, 0);
		float PlayerJump = 6.0f;
		float Velocity = 0.0f;

		glm::vec3 Front   = glm::vec3(1, 0, 0);
		glm::vec3 Up      = glm::vec3(0, 1, 0);
		glm::vec3 Right   = glm::vec3(0, 0, 1);
		glm::vec3 WorldUp = glm::vec3(0, 1, 0);

		glm::vec3 lastTickMove;
		// Euler Angles
		float Yaw = 90;
		float Pitch = 10;
		// Camera options
		float MovementSpeed;
		float MouseSensitivity;
		float Zoom;

		glm::vec3 displacement;

		Player();

		// Constructor with vectors
		Player(glm::vec3 position, glm::vec3 up, float yaw, float pitch );

		// Constructor with scalar values
		Player(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);

		// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
		glm::mat4 GetViewMatrix()
		{
			return glm::lookAt(Position + this->displacement, Position + this->displacement + Front, Up);
		}

		void updateCameraVectors();
};

