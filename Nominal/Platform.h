#pragma once
#include "gl/glm/vec2.hpp"
#include <vector>

#include "Triangle.h"

class Platform
{
public:
	Platform();
	~Platform();

	glm::vec2 init_position;
	glm::vec2 current_position;
	glm::vec2 end_position;
	glm::vec2 move_vec;
	float speed;
	float cycle = 0.0f;
	int direction = 1;

	std::string model_name = "";
	int VAO, VBO, modelVAO, modelVBO, vertexCount;
	float modelVertices[10000];
	std::vector<Triangle> solid_list;
	std::vector<float> vertexValues;
};

