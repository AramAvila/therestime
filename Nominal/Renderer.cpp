#include "Renderer.h"



Renderer::Renderer()
{

	this->setUpShaders();

	//this->setUpBufferObjects();
}


Renderer::~Renderer()
{
	glDeleteVertexArrays(1, &this->VAO);
	glDeleteBuffers(1, &this->VBO);
	glDeleteBuffers(1, &this->EBO);

	glfwTerminate();
}


bool Renderer::setUpShaders() {
	this->active_shader.buildShader(this->vertex_shader, this->fragment_shader);
	this->active_text_shader.buildShader(this->text_vertex_shader, this->text_fragment_shader);
	return 0;
}

bool Renderer::setUpBufferObjects() {

	float vertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};
	// world space positions of our cubes
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);

	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);


	// load and create a texture 
	// -------------------------
	//unsigned int texture1, texture2;
	// texture 1
	// ---------
	glGenTextures(1, &this->texture1);
	glBindTexture(GL_TEXTURE_2D, this->texture1);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrChannels;

	//stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.

	unsigned char *data = stbi_load("textures/container.jpg", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
	// texture 2
	// ---------
	glGenTextures(1, &this->texture2);
	glBindTexture(GL_TEXTURE_2D, this->texture2);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	data = stbi_load("textures/awesomeface.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		// note that the awesomeface.png has transparency and thus an alpha channel, so make sure to tell OpenGL the data type is of GL_RGBA
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

	// tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
	// -------------------------------------------------------------------------------------------
	this->active_shader.Use();
	this->active_shader.setInt("texture1", 0);
	this->active_shader.setInt("texture2", 1);
	return 0;

}

void Renderer::renderWelcomeScreen()
{

}

void Renderer::renderLevelInitalScreen()
{
}

int Renderer::renderScene(SceneManager * scene, int render_mode)
{

	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, this->SCR_WIDTH, this->SCR_HEIGHT);

	this->active_shader.Use();

	glm::mat4 projection = glm::perspective(glm::radians(scene->active_player.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	glm::mat4 view = scene->active_player.GetViewMatrix();

	this->active_shader.setMat4("projection", projection);
	this->active_shader.setMat4("view", view);

	int vertexColorLocation = glGetUniformLocation(this->active_shader.Program, "ourColor");
	glUseProgram(this->active_shader.Program);

	float mod = 0;
	int count = 0;

	glm::vec3 level_color_even(250, 124, 108);
	glm::vec3 level_color_odd(250, 175, 100);

	glm::vec3 distorted_level_color_even(150, 150, 150);
	glm::vec3 distorted_level_color_odd(100, 100, 100);

	glm::vec3 flatter_color_even(35, 173, 173);
	glm::vec3 flatter_color_odd(100, 250, 149);

	glm::vec3 goal_color_even(75, 179, 59);
	glm::vec3 goal_color_odd(129, 255, 109);

	glm::vec3 platform_color_even(255, 238, 0);
	glm::vec3 platform_color_odd(179, 167, 0);

	glm::vec3 deathSurface_color_even(255, 10, 10);
	glm::vec3 deathSurface_color_odd(255, 50, 50);

	level_color_even = level_color_even / 255.0f;
	level_color_odd = level_color_odd / 255.0f;

	distorted_level_color_even = distorted_level_color_even / 255.0f;
	distorted_level_color_odd = distorted_level_color_odd / 255.0f;

	flatter_color_even = flatter_color_even / 255.0f;
	flatter_color_odd = flatter_color_odd / 255.0f;

	goal_color_even = goal_color_even / 255.0f;
	goal_color_odd = goal_color_odd / 255.0f;

	platform_color_even = platform_color_even / 255.0f;
	platform_color_odd = platform_color_odd / 255.0f;

	deathSurface_color_even = deathSurface_color_even / 255.0f;
	deathSurface_color_odd = deathSurface_color_odd / 255.0f;

	for (std::vector<Level>::iterator instant = scene->timeline.begin(); instant != scene->timeline.end(); instant++)
	{
		//Per ressaltar l'instant en el que ens trobem
		if (scene->active_player.Position.z - instant->absPosition.z > 0 && scene->active_player.Position.z - instant->absPosition.z < 0.1) {
			mod = 0.3;
		}
		else {
			mod = 0;
		}

		bool distort = false;
		for (int i = 0; i < instant->distortion_init_list.size(); i++)
		{
			if (count > instant->distortion_init_list.at(i) && count < instant->distortion_end_list.at(i)) {
				distort = true;
			}
		}

		/*
		* ======================================================================
		* DIBUIXEM EL NIVELL
		* ======================================================================
		*/
		if (distort) {
			if (count % 2 == 0) {
				glUniform4f(vertexColorLocation, distorted_level_color_even.x, distorted_level_color_even.y, distorted_level_color_even.z, 1.0f);
			}
			else {
				glUniform4f(vertexColorLocation, distorted_level_color_odd.x, distorted_level_color_odd.y, distorted_level_color_odd.z, 1.0f);
			}
		}
		else {
			if (count % 2 == 0) {
				glUniform4f(vertexColorLocation, level_color_even.x, level_color_even.y, level_color_even.z, 1.0f);
			}
			else {
				glUniform4f(vertexColorLocation, level_color_odd.x, level_color_odd.y, level_color_odd.z, 1.0f);
			}
		}
		glBindVertexArray(instant->VAO);
		glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
		model = glm::translate(model, instant->absPosition);
		this->active_shader.setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, instant->vertexCount);


		/*
		* ======================================================================
		* DIBUIXEM AL FLATTER
		* ======================================================================
		*/
		if (
			(scene->instant_to_update <= 0 || count < scene->instant_to_update) 
			&& (
				!scene->flatter_completed || 
				(scene->flatter_completed && count < scene->instant_completed )
				)
			){

			if (distort) {
				if (count % 2 == 0) {
					glUniform4f(vertexColorLocation, distorted_level_color_even.x, distorted_level_color_even.y, distorted_level_color_even.z, 1.0f);
				}
				else {
					glUniform4f(vertexColorLocation, distorted_level_color_odd.x, distorted_level_color_odd.y, distorted_level_color_odd.z, 1.0f);
				}
			}
			else {
				if (count % 2 == 0) {
					glUniform4f(vertexColorLocation, flatter_color_even.x + mod, flatter_color_even.y + mod, flatter_color_even.z + mod, 1.0f);
				}
				else {
					glUniform4f(vertexColorLocation, flatter_color_odd.x + mod, flatter_color_odd.y + mod, flatter_color_odd.z + mod, 1.0f);
				}
			}


			glBindVertexArray(instant->flatter_VAO);
			model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			model = glm::translate(model, glm::vec3(instant->flatter_position.x, instant->flatter_position.y, instant->absPosition.z));
			if (instant->flatter_status == FLATTER_STATUS::DEAD) {
				model = glm::scale(model, glm::vec3(0.5f, 0.5f, 1));
				if (distort) {
					if (count % 2 == 0) {
						glUniform4f(vertexColorLocation, (distorted_level_color_even.x) / 2.0f, (distorted_level_color_even.y) / 2.0f, (distorted_level_color_even.z) / 2.0f, 1.0f);
					}
					else {
						glUniform4f(vertexColorLocation, (distorted_level_color_odd.x) / 2.0f, (distorted_level_color_odd.y) / 2.0f, (distorted_level_color_odd.z) / 2.0f, 1.0f);
					}
				}
				else {
					if (count % 2 == 0) {
						glUniform4f(vertexColorLocation, (flatter_color_even.x + mod) / 2.0f, (flatter_color_even.y + mod) / 2.0f, (flatter_color_even.z + mod) / 2.0f, 1.0f);
					}
					else {
						glUniform4f(vertexColorLocation, (flatter_color_odd.x + mod) / 2.0f, (flatter_color_odd.y + mod) / 2.0f, (flatter_color_odd.z + mod) / 2.0f, 1.0f);
					}
				}

			}
			this->active_shader.setMat4("model", model);

			glDrawArrays(GL_TRIANGLES, 0, instant->flatter_vertexCount);


			/*
			* ======================================================================
			* DIBUIXEM LA FLETXA D'ACCIONS SI CAL
			* ======================================================================
			*/

			ACTION_LIST action = scene->action_list.at(count);
			ORDER_LIST order = scene->order_list.at(count);

			if (count % 2 == 0) {
				glUniform4f(vertexColorLocation, flatter_color_even.x, flatter_color_even.y, flatter_color_even.z, 1.0f);
			}
			else {
				glUniform4f(vertexColorLocation, flatter_color_odd.x, flatter_color_odd.y, flatter_color_odd.z, 1.0f);
			}

			glBindVertexArray(instant->arrow_VAO);
			model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			glm::mat4 model_mod = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			model = glm::translate(model, glm::vec3(instant->flatter_position.x + instant->flatter_center.x, instant->flatter_position.y + instant->flatter_center.y * 2.2f, instant->absPosition.z));

			if (action != ACTION_LIST::ACTION_NONE && !distort && instant->flatter_status != FLATTER_STATUS::DEAD) {
				switch (action)
				{
				case JUMP:
					//model_mod = glm::rotate(model, 90.0f, glm::vec3(0, 0, 1));
					model_mod = model;
					break;
				case ACTION_NONE:
					model_mod = model;
					break;
				default:
					break;
				}
				this->active_shader.setMat4("model", model_mod);
				glDrawArrays(GL_TRIANGLES, 0, instant->arrow_vertexCount);
			}

			if (order != ORDER_LIST::ORDER_NONE && !distort && instant->flatter_status != FLATTER_STATUS::DEAD) {

				switch (order)
				{
				case FORWARDS:
					model_mod = glm::rotate(model, 3.1416f / 2.0f, glm::vec3(0, 0, -1));
					break;
				case BACKWARDS:
					model_mod = glm::rotate(model, 3.1416f / 2.0f, glm::vec3(0, 0, 1));
					break;
				case LEFT:
					break;
				case RIGHT:
					break;
				case STOP:
					model_mod = glm::rotate(model, 3.1416f, glm::vec3(0, 0, 1));
					break;
				case ORDER_NONE:
					model_mod = model;
					break;
				default:
					break;
				}

				this->active_shader.setMat4("model", model_mod);
				glDrawArrays(GL_TRIANGLES, 0, instant->arrow_vertexCount);
			}
		}


		/*
		* ======================================================================
		* DIBUIXEM LES PLATAFORMES
		* ======================================================================
		*/
		for (std::vector<Platform>::iterator pl = instant->platform_list.begin(); pl != instant->platform_list.end(); pl++) {

			if (distort) {
				if (count % 2 == 0) {
					glUniform4f(vertexColorLocation, distorted_level_color_even.x, distorted_level_color_even.y, distorted_level_color_even.z, 1.0f);
				}
				else {
					glUniform4f(vertexColorLocation, distorted_level_color_odd.x, distorted_level_color_odd.y, distorted_level_color_odd.z, 1.0f);
				}
			}
			else {
				if (count % 2 == 0) {
					glUniform4f(vertexColorLocation, platform_color_even.x, platform_color_even.y, platform_color_even.z, 1.0f);
				}
				else {
					glUniform4f(vertexColorLocation, platform_color_odd.x, platform_color_odd.y, platform_color_odd.z, 1.0f);
				}
			}

			glBindVertexArray(pl->VAO);
			glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			model = glm::translate(model, glm::vec3(pl->current_position.x, pl->current_position.y, instant->absPosition.z));
			this->active_shader.setMat4("model", model);
			glDrawArrays(GL_TRIANGLES, 0, pl->vertexCount);
		}

		/*
		* ======================================================================
		* DIBUIXEM LES SUPERF�CIES MORTALS
		* ======================================================================
		*/
		for (std::vector<DeathSurface>::iterator ds = instant->deathSurface_list.begin(); ds != instant->deathSurface_list.end(); ds++) {

			if (count % 2 == 0) {
				glUniform4f(vertexColorLocation, deathSurface_color_even.x, deathSurface_color_even.y, deathSurface_color_even.z, 1.0f);
			}
			else {
				glUniform4f(vertexColorLocation, deathSurface_color_odd.x, deathSurface_color_odd.y, deathSurface_color_odd.z, 1.0f);
			}

			glBindVertexArray(ds->VAO);
			glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			model = glm::translate(model, glm::vec3(0, 0, instant->absPosition.z));
			this->active_shader.setMat4("model", model);
			glDrawArrays(GL_TRIANGLES, 0, ds->vertexCount);
		}

		count++;
	}

	/*
	* ======================================================================
	* DIBUIXEM LA META
	* ======================================================================
	* S'ha de dibuixar l'�ltim perqu� t� transpar�ncia
	*/
	count = 0;
	glm::mat4 model = glm::mat4(1.0f);
	float transparency = 0.3f;
	glm::vec3 scale = glm::vec3(0.2f, 0.5f, 1);
	if (scene->flatter_completed || scene->player_completed) {
		transparency = 0.6f;
		scale = glm::vec3(0.6f, 0.7f, 1);
	}
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (std::vector<Level>::iterator instant = scene->timeline.begin(); instant != scene->timeline.end(); instant++)
	{
		if (count % 2 == 0) {
			glUniform4f(vertexColorLocation, goal_color_even.x, goal_color_even.y, goal_color_even.z, transparency);
		}
		else {
			glUniform4f(vertexColorLocation, goal_color_odd.x, goal_color_odd.y, goal_color_odd.z, transparency);
		}
		glBindVertexArray(instant->flatter_VAO);
		model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
		model = glm::translate(model, glm::vec3(instant->goal_position.x, instant->goal_position.y, instant->absPosition.z));
		model = glm::scale(model, scale);
		this->active_shader.setMat4("model", model);

		glDrawArrays(GL_TRIANGLES, 0, instant->flatter_vertexCount);
		count++;
	}
	glDisable(GL_BLEND);

	if (render_mode == 2) {
		count = 0;
		for (std::vector<Level>::iterator instant = scene->timeline.begin(); instant != scene->timeline.end(); instant++)
		{
			for each (std::pair<glm::vec3, std::string> item in instant->level_text_list)
			{
				if (count == item.first.z) {
					this->RenderText(this->active_text_shader, projection, view, item.second, glm::vec3(item.first.x, item.first.y, item.first.z*0.1f), 0, 0, 0.002f, glm::vec3(1, 1, 1));
				}
			}
			count++;
		}
	}

	if (render_mode == 0) {
		this->RenderText(this->active_text_shader, projection, view, "There's time", glm::vec3(5, 4, 10), 0, 0, 0.02f, glm::vec3(1, 1, 1));
		this->RenderText(this->active_text_shader, projection, view, "Aram Avila Salvado", glm::vec3(5, 3.3f, 10), 0, 0, 0.008f, glm::vec3(1, 1, 1));
		this->RenderText(this->active_text_shader, projection, view, "TFG - Universitat oberta de catalunya", glm::vec3(5, 3.0f, 10), 0, 0, 0.007f, glm::vec3(1, 1, 1));
		this->RenderText(this->active_text_shader, projection, view, "Prem qualsevol tecla per comencar...", glm::vec3(5, 2.5f, 10), 0, 0, 0.004f, glm::vec3(1, 1, 1));
	}

	if (render_mode == 3) {
		glm::vec3 text_position = scene->active_player.Position + scene->active_player.Front * 3.0f - scene->active_player.Right * 1.0f+ scene->active_player.Up;

		this->RenderText(this->active_text_shader, projection, view, "Nivell completat!", text_position, 0, 0, 0.003f, glm::vec3(1, 1, 1));
		this->RenderText(this->active_text_shader, projection, view, "Prem qualsevol tecla per continuar", text_position - scene->active_player.Up * 0.2f, 0, 0, 0.001f, glm::vec3(1, 1, 1));
	}

	if (render_mode == 4) {
		glm::vec3 text_position = scene->active_player.Position + scene->active_player.Front * 3.0f - scene->active_player.Right * 1.0f + scene->active_player.Up;

		this->RenderText(this->active_text_shader, projection, view, "Has completat tots els nivells!", text_position, 0, 0, 0.003f, glm::vec3(1, 1, 1));
		this->RenderText(this->active_text_shader, projection, view, "Prem qualsevol tecla per tornar a comencar o ESC per sortir", text_position - scene->active_player.Up * 0.2f, 0, 0, 0.001f, glm::vec3(1, 1, 1));
	}

	glfwSwapBuffers(this->window);

	return 0;
}


void Renderer::renderSceneEndScreen()
{
}

void Renderer::renderEndGame()
{
}

void Renderer::updateLevelModel(Level * current_level) {

	//Level
	GLuint modelVAO, modelVBO;

	glGenVertexArrays(1, &modelVAO);
	glGenBuffers(1, &modelVBO);
	// Fill buffer
	glBindBuffer(GL_ARRAY_BUFFER, modelVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(current_level->modelVertices), current_level->modelVertices, GL_STATIC_DRAW);
	// Link vertex attributes
	glBindVertexArray(modelVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); //(vertex atribute array to store, number of values, isNormalized?, size of vertex data, offset from start of vertex data)
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	current_level->VAO = modelVAO;
	current_level->VBO = modelVBO;


	//Player
	GLuint flatter_modelVAO, flatter_modelVBO;

	glGenVertexArrays(1, &flatter_modelVAO);
	glGenBuffers(1, &flatter_modelVBO);
	// Fill buffer
	glBindBuffer(GL_ARRAY_BUFFER, flatter_modelVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(current_level->flatter_modelVertices), current_level->flatter_modelVertices, GL_STATIC_DRAW);
	// Link vertex attributes
	glBindVertexArray(flatter_modelVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); //(vertex atribute array to store, number of values, isNormalized?, size of vertex data, offset from start of vertex data)
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	current_level->flatter_VAO = flatter_modelVAO;
	current_level->flatter_VBO = flatter_modelVBO;

	//Plattforms

	for (std::vector<Platform>::iterator pl = current_level->platform_list.begin(); pl != current_level->platform_list.end(); pl++) {
		GLuint pl_modelVAO, pl_modelVBO;

		glGenVertexArrays(1, &pl_modelVAO);
		glGenBuffers(1, &pl_modelVBO);
		// Fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, pl_modelVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(pl->modelVertices), pl->modelVertices, GL_STATIC_DRAW);
		// Link vertex attributes
		glBindVertexArray(pl_modelVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); //(vertex atribute array to store, number of values, isNormalized?, size of vertex data, offset from start of vertex data)
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		pl->VAO = pl_modelVAO;
		pl->VBO = pl_modelVBO;
	}

	for (std::vector<DeathSurface>::iterator ds = current_level->deathSurface_list.begin(); ds != current_level->deathSurface_list.end(); ds++) {
		GLuint ds_modelVAO, ds_modelVBO;

		glGenVertexArrays(1, &ds_modelVAO);
		glGenBuffers(1, &ds_modelVBO);
		// Fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, ds_modelVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(ds->modelVertices), ds->modelVertices, GL_STATIC_DRAW);
		// Link vertex attributes
		glBindVertexArray(ds_modelVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); //(vertex atribute array to store, number of values, isNormalized?, size of vertex data, offset from start of vertex data)
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		ds->VAO = ds_modelVAO;
		ds->VBO = ds_modelVBO;
	}

	//Arrow
	GLuint arrow_modelVAO, arrow_modelVBO;

	glGenVertexArrays(1, &arrow_modelVAO);
	glGenBuffers(1, &arrow_modelVBO);
	// Fill buffer
	glBindBuffer(GL_ARRAY_BUFFER, arrow_modelVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(current_level->arrow_modelVertices), current_level->arrow_modelVertices, GL_STATIC_DRAW);
	// Link vertex attributes
	glBindVertexArray(arrow_modelVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0); //(vertex atribute array to store, number of values, isNormalized?, size of vertex data, offset from start of vertex data)
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	current_level->arrow_VAO = arrow_modelVAO;
	current_level->arrow_VBO = arrow_modelVBO;
}

void Renderer::setUpTextRendering() {
	// FreeType
	FT_Library ft;
	// All functions return a value different than 0 whenever an error occurred
	if (FT_Init_FreeType(&ft))
		std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;

	// Load font as face
	FT_Face face;
	if (FT_New_Face(ft, "fonts/arial.ttf", 0, &face))
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;

	// Set size to load glyphs as
	FT_Set_Pixel_Sizes(face, 0, 48);

	// Disable byte-alignment restriction
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Load first 128 characters of ASCII set
	for (GLubyte c = 0; c < 128; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};
		Characters.insert(std::pair<GLchar, Character>(c, character));
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	// Destroy FreeType once we're finished
	FT_Done_Face(face);
	FT_Done_FreeType(ft);


	// Configure VAO/VBO for texture quads
	glGenVertexArrays(1, &this->text_VAO);
	glGenBuffers(1, &this->text_VBO);
	glBindVertexArray(this->text_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->text_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

/// <summary>
/// Renders the text.
/// </summary>
/// <param name="shader">The shader.</param>
/// <param name="text">The text.</param>
/// <param name="abs_pos">The absolute position of the text.</param>
/// <param name="x">The x offset form the absolute position.</param>
/// <param name="y">The y offset form the absolute position</param>
/// <param name="scale">The scale.</param>
/// <param name="color">The color.</param>
void Renderer::RenderText(Shader &shader, glm::mat4 projection, glm::mat4 view, std::string text, glm::vec3 abs_pos, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
{
	shader.Use();

	shader.setMat4("projection", projection);
	shader.setMat4("view", view);

	glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
	model = glm::translate(model, abs_pos);
	model = glm::scale(model, glm::vec3(-1, 1, 1));
	shader.setMat4("model", model);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Activate corresponding render state
	glUniform3f(glGetUniformLocation(shader.Program, "textColor"), color.x, color.y, color.z);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(this->text_VAO);

	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = Characters[*c];

		GLfloat xpos = x + ch.Bearing.x * scale;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

		GLfloat w = ch.Size.x * scale;
		GLfloat h = ch.Size.y * scale;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, this->text_VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);
}

GLuint Renderer::loadTexture(GLchar const * path)
{
	// Generate texture ID and load texture data
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height, nrChannels;

	unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);

	return textureID;
}