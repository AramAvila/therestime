#pragma once
#include "gl/glm/vec2.hpp"
#include <vector>

#include "Triangle.h"

class DeathSurface
{
public:
	DeathSurface();
	~DeathSurface();

	std::string model_name = "";
	int VAO, VBO, modelVAO, modelVBO, vertexCount;
	float modelVertices[10000];
	std::vector<Triangle> solid_list;
	std::vector<float> vertexValues;
};

