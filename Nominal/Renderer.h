#pragma once

#include <vector>
#include <fstream>

// GLEW
#define GLEW_STATIC
#include "GL/glew.h"

// GLFW
#include "GL/glfw3.h"
#include "gl/glut.h"

#include "gl/glm/glm.hpp"
#include "gl/glm/gtc/matrix_transform.hpp"
#include "gl/glm/gtc/type_ptr.hpp"

//#include "include\ft2build.h"
//#include FT_FREETYPE_H

#include "shaders\Shader.h"
#include "SceneManager.h"
#include "Player.h"

//#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Level.h"
#include "Hud.h"

#include <ft2build.h>
#include FT_FREETYPE_H  

class Renderer
{
public:

	struct Character {
		GLuint TextureID;   // ID handle of the glyph texture
		glm::ivec2 Size;    // Size of glyph
		glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
		GLuint Advance;    // Horizontal offset to advance to next glyph
	};

	static const int SCR_WIDTH = 1600;
	static const int SCR_HEIGHT = 900;

	GLuint VBO, VAO, EBO, texture1, texture2, vertex_count;

	const GLchar * vertex_shader = "shaders/default_shader.vs";
	const GLchar * fragment_shader = "shaders/default_shader.frag";

	const GLchar * text_vertex_shader = "shaders/text_default_shader.vs";
	const GLchar * text_fragment_shader = "shaders/text_default_shader.frag";


	std::map<GLchar, Character> Characters;
	GLuint text_VAO, text_VBO;

	GLFWwindow* window;

	std::vector<GLuint> vao_list;

	Renderer();
	~Renderer();

	Shader active_shader;
	Shader active_text_shader;

	bool setUpShaders();
	bool setUpBufferObjects();

	void renderWelcomeScreen();
	void renderLevelInitalScreen();
	int renderScene(SceneManager * scene, int render_mode);
	void renderSceneEndScreen();
	void renderEndGame();

	void updateLevelModel(Level * current_level);

	GLuint loadTexture(GLchar const * path);

	void setUpTextRendering();
	void RenderText(Shader &shader, glm::mat4 projection, glm::mat4 view, std::string text, glm::vec3 abs_pos, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

};

